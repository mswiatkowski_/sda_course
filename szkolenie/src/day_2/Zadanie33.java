package day_2;

public class Zadanie33 {

    public static void main(String[] args) {

        test(0, 15);
        System.out.println(fibo(7));

    }

    /**
     * Utwórz metodę, do której przekazujesz dwa parametry następnie wyświetlasz wszystkie liczby z podanego przedziału podzielne przez 3 i/lub 5
     * Dane wyświetl w formie:
     * > dla `0, 15` wyświetli:
     * >
     * ```0  <-- 3, 5
     * 1
     * 2
     * 3  <-- 3
     * 4
     * 5  <-- 5
     * 6
     * 7
     * 9  <-- 3
     * 10  <-- 5
     * 11
     * 12  <-- 3
     * 13
     * 14
     * 15  <-- 3, 5```
     */

    static void test(int num1, int num2) {

        if (num1 < num2) {

            int counter = 0;
            String out = "";

            for (int i = num1; i <= num2; i++) {

                out += counter + " <--- ";

                if (i % 3 == 0) {
                    out += 3;
                }

                if (i % 5 == 0) {
                    out += 5;
                }

                out += "\n";
                counter++;

            }

//            System.out.println(out);

        }

    }

    /**
     *
     * Utwórz metodę, do której przekazujesz jeden parametr i zwracasz wybrany element Fibonacciego:
     * > dla `7` zwróci `13` bo `1, 1, 2, 3, 5, 8, 13`
     * >
     * > dla `13` zwróci `233` bo `1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233`
     *
     */

    static int fibo(int num) {

        return num;

    }
}
