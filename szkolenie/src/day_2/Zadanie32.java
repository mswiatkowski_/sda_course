package day_2;

public class Zadanie32 {

    /**
     * Utwórz metodę, do której przekazujesz jeden parametr i zwraca sumę wszystkich elementów od `1` do podanej liczby
     * > dla `3` zwróci `1 + 2 + 3 = 6`
     * >
     * > dla `5` zwróci `15` bo `1 + 2 + 3 + 4 + 5 = 15`
     * >
     * > dla `11` zwróci `66` bo `1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 = 66`
     */

    public static void main(String[] args) {

        System.out.println(test(11));
        test1(0, 5, 5);
        System.out.println();
        test1(0, 6, 2.5);
        System.out.println();
        range(0, 6, 2.5);

    }

    static int test(int num) {

        int val = 0;

        for (int i = 0; i <= num; i++) {

            val += i;

        }

        return val;

    }

    /**
     *
     * Utwórz metodę, do której przekazujesz trzy parametry (np `start`, `amount`, `jump`).
     * Następujące parametry odpowiadają za:
     * pierwszy - początkowa (pierwsza) wyświetlona wartość
     * drugi  - liczba wyświetlonych elementów
     * trzeci (double) - różnica pomiędzy kolejnymi elementami (“skok”)
     * > Dla `0, 5, 5` wyświetli `0, 5, 10, 15, 20` (zaczynamy od `0`, wyświetlamy `5` elementów a elementy różnią się od siebie o `5`)
     * >
     * > Dla `0, 6, 2.5` wyświetli `0.0, 2.5, 5.0, 7.5, 10.0, 12.5` (zaczynamy od `0`, wyświetlamy `6` elementów a elementy różnią się od siebie o `2.5`)
     *
     */

    static void test1(int start, int amount, double jump) {

        String out = "";
        for (int i = 0; i < amount ; i++) {
            out += (i * jump) + ", ";
        }
        System.out.printf(out + "\b\b");

    }

    static void range(int min, int n, double step) {
        for (int i = 0; i < n; i++) {
            System.out.print((min + i * step) + ", ");
        }
    }

}
