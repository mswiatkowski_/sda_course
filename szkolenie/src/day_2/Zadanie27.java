package day_2;

public class Zadanie27 {
    public static void main(String[] args) {

        System.out.println(calculate('+', 3, 4));
        System.out.println(calculate('-', 3, 4));
        System.out.println(calculate('*', 3, 4));
        System.out.println(calculate('/', 3, 4));

    }

    static double calculate(char num, int val1, int val2) {

        switch(num) {
            case '+':
                return val1 + val2;
            case '-':
                return val1 - val2;
            case '*':
                return val1 * val2;
            case '/':
                return (double) val1 / val2;
        }

        return 0;

    }

}
