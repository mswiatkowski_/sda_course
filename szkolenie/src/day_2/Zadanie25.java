package day_2;

public class Zadanie25 {
    public static void main(String[] args) {

        System.out.println(getMonth(11));

    }

    static String getMonth(int nr) {

        String out;

        switch (nr) {
            case 1:
                out = "Styczeń";
                break;
            case 2:
                out = "Luty";
                break;
            case 3:
                out = "Marzec";
                break;
            case 4:
                out = "Kwiecień";
                break;
            case 5:
                out = "Maj";
                break;
            case 6:
                out = "Czerwiec";
                break;
            case 7:
                out = "Lipiec";
                break;
            case 8:
                out = "Sierpień";
                break;
            case 9:
                out = "Wrzeiseń";
                break;
            case 10:
                out = "Październik";
                break;
            case 11:
                out = "Listopad";
                break;
            case 12:
                out = "Grudzień";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + nr);
        }

        return out;

    }

}
