package day_2;

public class Zadanie22 {
    public static void main(String[] args) {

        System.out.println(test(11));
        System.out.println(test(2));
        System.out.println(test(49));

    }

    static boolean test(int num1) {

        return ((num1 >= 10 && num1 <= 20) || (num1 >= 40 && num1 <= 50));

    }

}
