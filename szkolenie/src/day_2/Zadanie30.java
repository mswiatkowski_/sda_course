package day_2;

public class Zadanie30 {

    public static void main(String[] args) {

        System.out.println(test(1, 10, 3));
        System.out.println(test(2, 36, 4));

    }

    // Dla `1, 10, 3` wyświetli `1, 4, 7, 10`

    static String test(int start, int end, int jump) {

        String out = "";

        for (int i = start; i <= end; i += jump) {
            out += i + ", ";
        }

        return out;

    }

}
