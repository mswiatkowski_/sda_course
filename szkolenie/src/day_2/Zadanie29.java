package day_2;

public class Zadanie29 {

    public static void main(String[] args) {

        int test = 256;

        System.out.println(pow(test));
        pow1(test);
        pow2(test);
        pow3(test);

    }

    static String pow(int num) {

        String out = "";

        for (int i = 0; i < num; i++) {

            double test = Math.pow(2, i);

            if (test > num) {
                break;
            }

            out += (int) test + ", ";

        }

        return out+"\b\b";

    }

    static void pow1(int num) {

        for (int i = 1; i <= num; i *= 2) {
            System.out.printf(i + ", ");
        }

        System.out.printf("\b\b");
        System.out.println();

    }

    static void pow2(int num) {

        int i = 1;

        while (i <= num) {

            System.out.printf(i + ", ");
            i *= 2;

        }

        System.out.println();

    }

    static void pow3(int num) {

        if(num < 1) {
            return;
        }

        int i = 1;

        do {

            System.out.printf(i + ", ");
            i *= 2;

        } while (i <= num);

    }

}
