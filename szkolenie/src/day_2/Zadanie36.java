package day_2;

public class Zadanie36 {

    public static void main(String[] args) {

        test(6);
        test2(6);

    }


    /**
     * Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę” nie przerywając numerowania
     * > Dla `4` wyświetli:
     * >
     * 1
     * 2 3
     * 4 5 6
     * 7 8 9 10```
     * >
     * > Dla `6` wyświetli:
     * >
     * 1
     * 2 3
     * 4 5 6
     * 7 8 9 10
     * 11 12 13 14 15
     * 16 17 18 19 20 21```
     */

    static void test(int value) {

        int out = 1;

        for (int i = 1; i <= value; i++) {

            for (int j = 1; j <= i; j++) {
                System.out.print(out + " ");
                out++;
            }

            System.out.println();
        }

    }

    /**
     *
     * Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę” wyrównaną do prawej strony
     * > Dla `5` wyświetli:
     * >
     * ```.....1
     * ....22
     * ...333
     * ..4444
     * .55555```
     *
     */

    static void test2(int value) {



    }

}
