package day_2;

public class Zadanie34 {

    public static void main(String[] args) {

        test(1, 6);
        test2(7);

    }

    /**
     *
     * Utwórz metodę, do której przekazujesz dwa parametry następnie wyświetlasz wszystkie liczby z podanego przedziału *I ICH DZIELNIKI*
     *
     * Dane wyświetl w formie:
     * > dla `1, 6` wyświetli:
     * >
     *  ```1  <--  1
     *  2  <--  1, 2,
     *  3  <--  1, 3,
     *  4  <--  1, 2, 4,
     *  5  <--  1, 5,
     *  6  <--  1, 2, 4, 6```
     *
     */

    static void test(int num1, int num2) {

        for (int i = num1; i <= num2; i++) {

            String out = "";

            for (int j = num1; j <= i ; j++) {

                if(i % j == 0) {
                    out += j + " ";
                }

            }

            System.out.println(i + " <-> " + out);

        }

    }


    /**
     *
     * Utwórz metodę, do której przekazujesz liczbę, a ona *ZWRACA* informację czy liczba jest PIERWSZA czy nie (tzn. podzielna przez 1 i samą siebie).
     *
     */

    static void test2(int value) {

        int out = 0;

        for (int i = 1; i <= value ; i++) {

            if (value % i == 0) {
                out++;
            }

        }

        if(out == 2) {
            System.out.println("Liczba pierwsza");
        } else {
            System.out.println("Liczba nie jest pierwsza");
        }

    }





}
