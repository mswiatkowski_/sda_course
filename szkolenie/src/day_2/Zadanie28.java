package day_2;

public class Zadanie28 {
    public static void main(String[] args) {

        System.out.println(count(-3)+"\b\b");

    }

    static String count(int num) {

        String out = "";

        for (int i = num; i <= 0; i++) {
            out += i + ", ";
        }

        for (int i = 0; i <= num; i++) {
            out += i + ", ";
        }

        return out;
    }

}
