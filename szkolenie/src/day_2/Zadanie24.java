package day_2;

public class Zadanie24 {
    public static void main(String[] args) {

        System.out.println(getTicket(40, 50));
        System.out.println(getTicket(65, 50));
        System.out.println(getTicket(70, 50));
        System.out.println(getTicket(141, 50));

    }

    static int getTicket(int speed, int limit) {

        if (speed <= limit) {

            return 0;

        } else {

            int value = (speed - limit) / 10;
            int ticket = value * 100;

            if (value % 10 > 0) {
                ticket += 100;
            }

            return ticket;

        }

    }

}