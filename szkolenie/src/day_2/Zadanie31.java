package day_2;

public class Zadanie31 {
    public static void main(String[] args) {

        pow(4);
        test(3, 100);
        test(4, 300);

    }

    static void pow(int num) {

        for (int i = 0; i <= num; i++) {

            System.out.print((i * 10) + ", ");

        }

        System.out.println();

    }

    static void test(int num1, int num2) {

        for (int j = 0; Math.pow(num1, j) < num2; j++) {

            System.out.println(j + " -> " + Math.pow(num1, j));

        }


    }

}
