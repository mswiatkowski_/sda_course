package day_2;

public class Zadanie35 {

    public static void main(String[] args) {

        test(6);
        test2(14);

    }

    /**
     *
     * Utwórz metodę, która przyjmuje *jeden* parametr (który jest liczbą wierszy i kolumn) oraz wyświetla tabliczkę mnożenia (znaki możesz oddzielać znakiem tabulacji `\t`).
     * > Dla `3` wyświetli:
     * >
     * 1   2   3
     * 2   4   6
     * 3   6   9```
     *
     *
     * > Dla `6` wyświetli:
     * >
     * 1   2   3   4   5   6
     * 2   4   6   8  10  12
     * 3   6   9  12  15  18
     * 4   8  12  16  20  24
     * 5  10  15  20  25  30
     * 6  12  18  24  30  36```
     *
     */

    static void test(int value) {

        for (int i = 1; i <= value ; i++) {

            for (int j = 1; j <= value ; j++) {

                System.out.print((i * j) + "\t");

            }

            System.out.println();

        }

    }


    /**
     *
     * Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę”
     * > Dla `4` wyświetli:
     * >
     * ```1
     * 12
     * 123
     * 1234```
     * >
     * > Dla `6` wyświetli:
     * >
     * ```1
     * 12
     * 123
     * 1234
     * 12345
     * 123456```
     *
     */

    static void test2(int value) {

        for (int i = 1; i <= value ; i++) {
            for (int j = 1; j <= i ; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

    }

}
