package day_2;

public class Zadanie26 {
    public static void main(String[] args) {

        System.out.println(getDays(12));

    }

    static int getDays(int month) {

        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                return 28;
            default:
                return 0;
        }

    }

}
