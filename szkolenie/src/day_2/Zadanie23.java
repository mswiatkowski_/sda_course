package day_2;

public class Zadanie23 {
    public static void main(String[] args) {

        System.out.println(test(6));
        System.out.println(test(22));
        System.out.println(test(25));
        System.out.println(test(33));

    }

    static int test(int num1) {

        if (num1 < 11) {
            return 11 - num1;
        } else if (num1 <= 22) {
            return 0;
        } else {
            return 22 - num1;
        }

    }

}
