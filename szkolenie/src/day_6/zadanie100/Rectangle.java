package day_6.zadanie100;

public class Rectangle {

    private int shortSide;
    private int longSide;

    Rectangle(int shortSide, int longSide) {
        this.shortSide = shortSide;
        this.longSide = longSide;
    }

    int square() {
        return shortSide * longSide;
    }

    int circuit() {
        return 2 * shortSide + 2 * longSide;
    }

    static boolean isTheSame(Rectangle... value3) {

        for (int i = 1; i < value3.length; i++) {
            if (value3[i].square() != value3[i - 1].square()) {
                return false;
            }
        }

        return true;

    }

    public int getShortSide() {
        return shortSide;
    }

    public int getLongSide() {
        return longSide;
    }
}
