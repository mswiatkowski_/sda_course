package day_6.zadanie100;

/**
 * Utwórz klasę `Rectangle` (`Prostokąt`) posiadającą
 * >*pola reprezentujące:*
 * >* dłuższy bok
 * >* krótszy bok
 * >
 * >*konstruktory oraz metody służące do:*
 * >* policzenia obwodu
 * >* policzenia pola powierzchni
 * >* porównania czy pola powierzchni dwóch przekazanych prostokątów są takie same (zwracająca wartość `boolean`)
 * >
 * >*Utwórz tablicę zawierającą 5 obiektów tej klasy i w pętli wyświetl zdania w formie*
 * >`10 x 20 = 200`
 */

public class Main {

    public static void main(String[] args) {

        Rectangle pole1 = new Rectangle(5, 10);
        Rectangle pole2 = new Rectangle(5,23);
        Rectangle pole3 = new Rectangle(17,12);
        Rectangle pole4 = new Rectangle(14,50);
        Rectangle pole5 = new Rectangle(12,6);

        Rectangle[] teb = {pole1, pole2, pole3, pole4, pole5};

        for (int i = 0; i < teb.length; i++) {
            System.out.println(String.format("%s x %s = %s", teb[i].getLongSide(), teb[i].getShortSide(), teb[i].square()));
        }


        System.out.println(Rectangle.isTheSame(pole1, pole2, pole3));

    }
}
