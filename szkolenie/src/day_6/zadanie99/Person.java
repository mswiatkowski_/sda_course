package day_6.zadanie99;

/**
 * *konstruktor*
 * >
 * >*metody służące do:*
 * >* przedstawienia się
 * (`Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
 * >* sprawdzenia czy osoba jest pełnoletnia
 */

public class Person {

    private String name;
    private String lastName;
    private int age;
    private boolean isMale;

    Person(String name, String lastName, int age, boolean isMale) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.isMale = isMale;
    }

    String introduce() {
        return String.format("Witaj, jestem %s %s, mam %s lat i jestem %s.",
                name, lastName, age,
                isMale ? "mężczyzną" : "kobietą");
    }

    boolean isAdult() {
        return age >= 18;
    }

    @Override
    public String toString() {
        return introduce() + (isAdult() ? " Jestem pełnoletni" : " Nie jestem pełnoletni");
    }
}
