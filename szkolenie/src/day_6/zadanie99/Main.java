package day_6.zadanie99;

public class Main {

    public static void main(String[] args) {

        Person person = new Person("Jan",
                "Kowalski",
                17,
                true);

        System.out.println(person.introduce());
        System.out.println(person.isAdult());

        Person person1 = new Person("Ewa",
                "Kowalska",
                23,
                false);

        System.out.println(person1.introduce());
        System.out.println(person1.isAdult());

        System.out.println(person1);


    }

}
