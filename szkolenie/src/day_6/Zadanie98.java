package day_6;

/**
 * Utwórz metodę, która przyjmuje ciąg znaków (który zawiera tylko liczby),
 * a następnie rozdziela elementy myślnikami. Separator ma stać w tych
 * miejscach by w danej grupie (pomiędzy myślnikami) były maksymalnie dwie liczby parzyste lub nieparzyste.
 * Dla `2222222` zwróci `22-22-22-2`
 * Dla `32933999293` zwróci `329-33-99-929-3`
 */


public class Zadanie98 {
    public static void main(String[] args) {

        String value = "32933999293";
        System.out.println(separator(value));

    }

    static String separator(String input) {

        String[] array = input.split("");
        int[] nums = new int[array.length];
        StringBuilder sb = new StringBuilder();
        int evencounter = 0;
        int oddcounter = 0;

        for (int i = 0; i < array.length; i++) {
            nums[i] = Integer.parseInt(array[i]);
        }

        for (int i = 0; i < nums.length; i++) {

            sb.append(nums[i]);

            if (nums[i] % 2 == 0) {
                evencounter += 1;
            } else {
                oddcounter += 1;
            }

            if (evencounter == 2 || oddcounter == 2) {
                sb.append("-");
                evencounter = 0;
                oddcounter = 0;
            }

        }

        if (evencounter == 0 && oddcounter == 0) {
            sb.deleteCharAt(sb.lastIndexOf("-"));
        }

        return sb.toString();

    }
}
