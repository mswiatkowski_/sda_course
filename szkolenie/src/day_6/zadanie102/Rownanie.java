package day_6.zadanie102;

/**
 * Utwórz klasę `Rownanie` służącą do policzenia rownania `a^2 + b^3 + c^4`. Klasa powinna zawierać:
 * >*pola:*
 * >* `a`, `b`, `c`
 * >
 * >*kostruktory:*
 * >* bezparametrowy
 * >* 3-parametrowy
 * >
 * >*metody:*
 * >* liczaca wartosc rownania
 * >* przyjmującą liczbę a następnie zwracająca informację (`boolean`)
 * czy wartość równania przekroczyła podaną liczbą (jako parametr)
 */

public class Rownanie {

    private int a;
    private int b;
    private int c;
    private double valueOfEq;

    Rownanie() {

    }

    Rownanie(int a, int b, int c) {

        this.a = a;
        this.b = b;
        this.c = c;

        this.valueOfEq = equationValue();

    }

    double equationValue() {
        return equationValue(2);
    }


    double equationValue(int val) {
        return Math.pow(a, val) + Math.pow(b, val + 1) + Math.pow(c, val + 2);
    }

    boolean isBigger(int value) {
        return valueOfEq < value;
    }

}
