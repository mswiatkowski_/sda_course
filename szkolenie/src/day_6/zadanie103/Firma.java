package day_6.zadanie103;

/**
 * Utwórz klasę `Firma` posiadającą
 * >*pola reprezentujące:*
 * > - nazwę firmy
 * > - rok założenia
 * > - tablicę pracowników
 * <p>
 * >*metody umożliwiające:*
 * > - zatrudnienie pracownika - jeśli to możliwe (jeśli nie, zwrócić wartość `false`)
 * > - zwolnienie pracownika
 * > - zmianę nazwy firmy
 */

public class Firma{

    private String nazwaFirmy;
    private int rokZalozenia;
    private Pracownicy[] pracownicy = new Pracownicy[4];

    Firma(String nazwaFirmy, int rokZalozenia) {
        this.nazwaFirmy = nazwaFirmy;
        this.rokZalozenia = rokZalozenia;
    }

    void setNazwaFirmy(String nazwaFirmy) {
        this.nazwaFirmy = nazwaFirmy;
    }

    boolean zatrudnijPracownika(Pracownicy pracownik) {
        for (int i = 0; i < pracownicy.length; i++) {
            if(pracownicy[i] == null) {
                pracownicy[i] = pracownik;
                return true;
            }
        }

        return false;

    }

    void zwolnijPracownika(Pracownicy pracownik) {
        for (int i = 0; i < pracownicy.length; i++) {
            if(pracownicy[i] == pracownik) {
                pracownicy[i] = null;
            }
        }
    }

    StringBuilder getPracownicy() {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < pracownicy.length; i++) {
            if(pracownicy[i] != null) {
                out.append(i).append(". ").append(pracownicy[i].getImie()).append(" ").append(pracownicy[i].getNazwisko()).append("\n");
            }
        }
        return out;
    }
}
