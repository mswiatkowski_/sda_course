package day_6.zadanie103;

public class Main {

    public static void main(String[] args) {

        Pracownicy pracownik1 = new Pracownicy("Jan", "Nowak", "A");
        Pracownicy pracownik2 = new Pracownicy("Edyta", "Nowak", "AA");
        Pracownicy pracownik3 = new Pracownicy("Marek", "Nowak", "AAA");

        Firma firma = new Firma("SDA", 1987);
        System.out.println(firma.getPracownicy());

        firma.zatrudnijPracownika(pracownik2);
        System.out.println(firma.getPracownicy());

        firma.zatrudnijPracownika(pracownik1);
        System.out.println(firma.getPracownicy());

        firma.zatrudnijPracownika(pracownik3);
        System.out.println(firma.getPracownicy());

        firma.zatrudnijPracownika(pracownik3);
        System.out.println(firma.getPracownicy());

        firma.zatrudnijPracownika(pracownik3);
        System.out.println(firma.getPracownicy());

        firma.zatrudnijPracownika(pracownik3);
        System.out.println(firma.getPracownicy());

        firma.zwolnijPracownika(pracownik1);
        System.out.println(firma.getPracownicy());

    }

}
