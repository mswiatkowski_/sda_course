package day_6.zadanie103;

public class Pracownicy {

    private String imie;
    private String nazwisko;
    private String dzial;

    Pracownicy(String imie, String nazwisko, String dzial) {

        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dzial = dzial;

    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getDzial() {
        return dzial;
    }
}
