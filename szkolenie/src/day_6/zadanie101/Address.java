package day_6.zadanie101;

/**
 * *pola reprezentujące:*
 * >* ulicę
 * >* numer domu
 * >* numer mieszkania
 * >* kod pocztowy
 * >* miasto
 * >
 * >*następujące kostruktory:*
 * >* bezparametrowy
 * >* 3-parametrowy (przyjmujący pierwsze trzy pola)
 * >* 5-parametrowy (przyjmujący wszystkie pola)
 * >
 * >*metody zwracające:*
 * * pełen adres
 * * tylko ulicę (wraz z numerami)
 * * informację czy przekazany (inny) adres znajduje się na tej samej ulicy
 */

public class Address {

    private String ulica;
    private int nrDomu;
    private int nrMieszkania;
    private String kodPocztowy;
    private String miasto;

    Address() {

    }

    public Address(String ulica, int nrDomum, int nrMieszkania) {

        this.ulica = ulica;
        this.nrDomu = nrDomum;
        this.nrMieszkania = nrMieszkania;

    }

    Address(String ulica, int nrDomum, int nrMieszkania, String kodPocztowy, String miasto) {

        this.ulica = ulica;
        this.nrDomu = nrDomum;
        this.nrMieszkania = nrMieszkania;
        this.kodPocztowy = kodPocztowy;
        this.miasto = miasto;

    }

    String returnAddress() {
        return String.format("%s\n%s %s", returnSomeData(), kodPocztowy, miasto);
    }

    String returnSomeData() {
        return String.format("ul. %s %s/%s", ulica, nrDomu, nrMieszkania);
    }

    boolean compareAddress(Address address) {

        return this.kodPocztowy.equals(address.kodPocztowy);

    }


}
