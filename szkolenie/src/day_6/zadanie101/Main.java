package day_6.zadanie101;

public class Main {

    public static void main(String[] args) {

        Address adres = new Address("Dąbrowskiego",
                12,
                23,
                "94-321",
                "Łódź");

        Address adres2 = new Address();

        System.out.println(adres.returnAddress());
        System.out.println(adres.compareAddress(adres2));

    }

}
