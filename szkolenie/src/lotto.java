/*
Napisz program, który wczyta od uzytkownika liczbe naturalna n.
Nastepnie program n-razy symuluje losowanie 5 liczb z 45 zapisujac
kazda wylosowana serie posortowana rosnaco i w odzdzielnej lini.


 */

import java.util.*;

public class lotto {

    public static void main(String[] args) {

        lotto();

    }

    static void lotto() {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj liczbę losowań: ");
        int number = scanner.nextInt();

        Random rand = new Random();

        for (int i = 0; i < number; i++) {

            List<Integer> out = new ArrayList<>();

            for (int j = 0; j < 5; j++) {

                out.add(rand.nextInt(46));

            }

            Collections.sort(out);
            printList(out);

        }

    }

    static void printList(List<Integer> lista) {

        for (int i = 0; i < lista.size(); i++) {
            System.out.print(lista.get(i) + " ");
        }
        System.out.println();

    }

}
