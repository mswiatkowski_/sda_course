package day_14.Zadanie167;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * *ZADANIE #167 - interfejs `Comparable`*
 * Utwórz klasę `Dziecko` z polami `imie` i `wiek` oraz zaimplementuj interfejs `Comparable` (edited)
 */

public class Main {

    public static void main(String[] args) {

        Dziecko d1 = new Dziecko("Ala", 13, "test");
        Dziecko d2 = new Dziecko("Marek", 10, "dhwj");
        Dziecko d3 = new Dziecko("Basia", 11, "ddwdw");
        Dziecko d4 = new Dziecko("Arek", 5, "dwdwdw");
        Dziecko d5 = new Dziecko("Jacek", 7, "dwdw");
        Dziecko d6 = new Dziecko("Brajanek", 7, "Dwdw");
        Dziecko d7 = new Dziecko("Anna", 12, "dwdwdwd");
        Dziecko d8 = new Dziecko("Maciej", 4, "wewewe");

        List<Dziecko> dzieci = new ArrayList<>();
        dzieci.add(d1);
        dzieci.add(d2);
        dzieci.add(d3);
        dzieci.add(d4);
        dzieci.add(d5);
        dzieci.add(d6);
        dzieci.add(d7);
        dzieci.add(d8);

        System.out.println(dzieci);
        Collections.sort(dzieci);
        System.out.println(dzieci);

    }

}
