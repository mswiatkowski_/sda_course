package day_14.Zadanie167;

/**
 * *ZADANIE #167 - interfejs `Comparable`*
 * Utwórz klasę `Dziecko` z polami `imie` i `wiek` oraz zaimplementuj interfejs `Comparable` (edited)
 */

public class Dziecko implements Comparable<Dziecko> {

    private String imie;
    private String nazwisko;
    private int wiek;

    public Dziecko(String imie, int wiek, String nazwisko) {
        this.imie = imie;
        this.wiek = wiek;
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    @Override
    public String toString() {
        return "{" +
                "'" + imie + '\'' +
                "'" + nazwisko + '\'' +
                ", " + wiek +
                '}';
    }

    @Override
    public int compareTo(Dziecko dziecko) {
        // jesli wiek jest równy
        if (this.wiek == dziecko.wiek) {
            // porownujemy imie i zwracamy wynik porownywania imion
            return this.imie.compareTo(dziecko.imie);
        }

        return this.wiek - dziecko.wiek;
    }

}
