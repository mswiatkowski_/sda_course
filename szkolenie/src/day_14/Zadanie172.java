package day_14;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * *ZADANIE #172*
 * Utwórz metodę która przyjmuje jako parametr godzinę (w formie Stringa)
 * w postaci `13:22:18` (tj. `godzina-minuta-sekunda`) oraz zwróci tekst
 * w postaci `Mamy 22 minuty po godzinie 13-tej`
 */

public class Zadanie172 {

    public static void main(String[] args) {

        System.out.println(metoda("22:32:21"));
        System.out.println(metoda("13:03:11"));
    }

    private static String metoda(String time) {

        DateTimeFormatter formatIn = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime timeIn = LocalTime.parse(time, formatIn);

        int minutes = timeIn.getMinute();
        int hour = timeIn.getHour();

        return String.format("Mamy %s minuty po godzinie %s",
                minutes,
                hour
        );
    }

}
