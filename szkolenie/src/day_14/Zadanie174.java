package day_14;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * *ZADANIE #174*
 * Utwórz metodę która przyjmuje jako parametr datę (w formie Stringa)
 * w postaci `24-03-2017` lub `24.03.2017` oraz ma zwrócić informację który to jest dzień roku.
 */

public class Zadanie174 {

    public static void main(String[] args) {

        System.out.println(method("23-04-2019"));
        System.out.println(method("23.04.2019"));

    }

    private static int method (String data) {

        DateTimeFormatter formatIn = DateTimeFormatter.ofPattern("[dd-MM-yyyy][dd.MM.yyyy]");
        LocalDate dateIn = LocalDate.parse(data, formatIn);

        return dateIn.getDayOfYear();

    }
}
