package day_14.Zadanie169;

/**
 * *ZADANIE #169*
 * Utwórz klasę `Komputer` z metodami `start()` i `prepare()`, które wyświetlają jakieś komunikaty tekstowe.
 * Metoda `start()` ma być wywoływana w konstruktorze.
 *
 * Utwórz klasę `NowyKomputer()` która dziedziczy po klasie `Komputer()` i ma inne treści wyświetlanych metod.
 */

public class NowyKomputer extends Komputer {

    @Override
    void start() {
        System.out.println("STart z dziedziczenia ...");
    }

    @Override
    void prepeare() {
        System.out.println("Prepare z dziedziczenia ...");
    }

}
