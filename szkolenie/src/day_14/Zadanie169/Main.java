package day_14.Zadanie169;

public class Main {

    public static void main(String[] args) {

        Komputer c1 = new Komputer();
        c1.prepeare();

        NowyKomputer c2 = new NowyKomputer();
        c2.prepeare();

        Komputer c3 = new Komputer(){
            @Override
            void start() {
                System.out.println("Anonimowy start ...");
            }

            @Override
            void prepeare() {
                System.out.println("anonimowy prepare");
            }
        };

        c3.prepeare();

        new NowyKomputer() {
            @Override
            void start() {
                System.out.println("yyyyyy");
            }
        }.prepeare();

    }

}
