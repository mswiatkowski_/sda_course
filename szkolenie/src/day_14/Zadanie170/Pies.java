package day_14.Zadanie170;

public class Pies extends Zwierze {

    Pies() {
        System.out.println("Jestem pieskiem");
    }

    {
        System.out.println("1. Blok inicjalizacyjny pieska");
    }


    {
        System.out.println("3. I jeszcze jeden ...");
    }

    {
        System.out.println("2. A to kolejny blok i.");
    }

    static {
        System.out.println("statyczny blok i. pieska.");
        imie = "Azor";
    }

}
