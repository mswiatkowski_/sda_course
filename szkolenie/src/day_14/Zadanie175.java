package day_14;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * *ZADANIE #175*
 * Utwórz metodę która przyjmuje *dwie* daty (w postaci `2018-04-07`) oraz zwraca liczbę dni różnicy pomiędzy nimi.
 */

public class Zadanie175 {

    public static void main(String[] args) {
        System.out.println(method("2019-11-11", "2019-12-11"));
    }

    private static long method(String data1, String data2) {

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate d1 = LocalDate.parse(data1, format);
        LocalDate d2 = LocalDate.parse(data2, format);

        return ChronoUnit.DAYS.between(d1, d2);

    }
}
