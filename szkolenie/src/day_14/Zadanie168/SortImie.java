package day_14.Zadanie168;

import day_14.Zadanie167.Dziecko;

import java.util.Comparator;

/**
 * *ZADANIE #168 - interfejs `Comparator`*
 * Utwórz 3 własne klasy które implementują interfejs `Comparator` i  będą służyły do sortowania obiektów klasy `Dziecko` (z *zadania #167*)
 *
 * Pierwsza ma sortować po imionach,
 * a druga po imieniu i nazwisku,
 * a trzecia po wieku
 */

public class SortImie implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {

        if (!(o1 instanceof Dziecko) || !(o2 instanceof Dziecko)) {
            return 0;
        }

        Dziecko d1 = (Dziecko) o1;
        Dziecko d2 = (Dziecko) o2;

        return d1.getImie().compareTo(d2.getImie());
    }

}
