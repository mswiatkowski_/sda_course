package day_14.Zadanie168;

import day_14.Zadanie167.Dziecko;

import java.util.Comparator;

public class SortWiek implements Comparator {

    private JakSortowac sort;

    SortWiek(JakSortowac sort) {
        this.sort = sort;
    }

    @Override
    public int compare(Object o1, Object o2) {

        if (!(o1 instanceof Dziecko) || !(o2 instanceof  Dziecko)) {
            return 0;
        }

        Dziecko d1 = (Dziecko) o1;
        Dziecko d2 = (Dziecko) o2;

        return (sort.equals(JakSortowac.MALEJACO))? d2.getWiek() - d1.getWiek() : d1.getWiek() - d2.getWiek();

    }

}

enum JakSortowac {
    MALEJACO, ROSNACO;
}
