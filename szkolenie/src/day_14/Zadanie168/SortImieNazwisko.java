package day_14.Zadanie168;

import day_14.Zadanie167.Dziecko;

import java.util.Comparator;

public class SortImieNazwisko implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {

        if (!(o1 instanceof Dziecko) || !(o2 instanceof Dziecko)) {
            return 0;
        }

        Dziecko d1 = (Dziecko) o1;
        Dziecko d2 = (Dziecko) o2;

        int sortImie = d1.getImie().compareTo(d2.getImie());

        if (sortImie == 0) {
            return d1.getNazwisko().compareTo(d2.getNazwisko());
        }

        return sortImie;
    }

}
