package day_14.Zadanie168;

import day_14.Zadanie167.Dziecko;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * *ZADANIE #168 - interfejs `Comparator`*
 * Utwórz 3 własne klasy które implementują interfejs `Comparator` i  będą służyły do sortowania obiektów klasy `Dziecko` (z *zadania #167*)
 *
 * Pierwsza ma sortować po imionach,
 * a druga po imieniu i nazwisku,
 * a trzecia po wieku
 */

public class Main {

    public static void main(String[] args) {

        Dziecko d1 = new Dziecko("Ala", 13, "test");
        Dziecko d2 = new Dziecko("Marek", 10, "dhwj");
        Dziecko d3 = new Dziecko("Basia", 11, "ddwdw");
        Dziecko d4 = new Dziecko("Anna", 5, "dwdwdw");
        Dziecko d5 = new Dziecko("Jacek", 7, "dwdw");
        Dziecko d6 = new Dziecko("Brajanek", 7, "Dwdw");
        Dziecko d7 = new Dziecko("Anna", 12, "derder");
        Dziecko d8 = new Dziecko("Maciej", 4, "wewewe");

        List<Dziecko> dzieci = new ArrayList<>();
        dzieci.add(d1);
        dzieci.add(d2);
        dzieci.add(d3);
        dzieci.add(d4);
        dzieci.add(d5);
        dzieci.add(d6);
        dzieci.add(d7);
        dzieci.add(d8);

        System.out.println(dzieci);
        dzieci.sort(new SortImie());
        System.out.println(dzieci);
        dzieci.sort(new SortWiek(JakSortowac.ROSNACO));
        System.out.println(dzieci);
        dzieci.sort(new SortImieNazwisko());
        System.out.println(dzieci);

    }


}
