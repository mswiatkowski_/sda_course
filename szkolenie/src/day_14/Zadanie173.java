package day_14;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * *ZADANIE #173*
 * Utwórz metodę która przyjmuje jako parametr datę
 * (w formie Stringa) w postaci `2017-03-24`
 * (tj. `rok-miesiac-dzień`) oraz zwróć datę (w formie Stringa)
 * w postaci `24.03.2017` (tj. `dzień.miesiąc.rok`)
 */

public class Zadanie173 {

    public static void main(String[] args) {

        System.out.println(method("2019-03-24"));
    }

    private static String method(String data) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(data, formatter);

        DateTimeFormatter outData = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return date.format(outData);

    }

}
