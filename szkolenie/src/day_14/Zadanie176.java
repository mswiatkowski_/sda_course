package day_14;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * STREAMY
 */

public class Zadanie176 {

    public static void main(String[] args) {
//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5(5252325);
//        metoda6();
//        metoda7();
        metoda8();
    }

    private static void metoda1() {

        IntStream.range(10, 20).forEach(
                liczba -> System.out.println(liczba)
        );

    }

    private static void metoda2() {

        IntStream.rangeClosed(10, 40).filter(
                liczba -> liczba % 3 == 0
        ).forEach(
                liczba -> System.out.println(liczba)
        );

    }

    private static void metoda3() {

        Random random = new Random();
        List<Integer> lista = random.ints(5, 120, 150).boxed().collect(
                Collectors.toList()
        );

        System.out.println(lista);

    }

    private static void metoda4() {

        int[] tabInt = {1, 2, 3, 4, 5, 6, 8, 7, 9, 0};
        List<String> list = Arrays.stream(tabInt).filter(
                value -> value % 2 == 0
        ).boxed().map(
                value -> Integer.toString(value)
        ).collect(Collectors.toList());

        System.out.println(list);
    }

    private static void metoda5(int value) {

        String[] str = String.valueOf(value).split("");

        List<Integer> list = Arrays.stream(str).map(
                v -> Integer.valueOf(v)
        ).collect(Collectors.toList());

        System.out.println(list);

    }

    private static void metoda6() {

        List<String> lista = Arrays.asList("ala", "", null, "ela", "", "test", "coś", null, "ola");

        List<String> newList = lista.stream().filter(
                v -> v != null
        ).filter(
                v -> !v.isEmpty()
        ).collect(Collectors.toList());

        System.out.println(newList);

    }

    private static void metoda7() {

        List<String> lista = Arrays.asList("Ala", "Ela", "testy", "Jacek", "Basia", "Arek");

        List<String> tt = lista.stream().filter(
                v -> v.matches(".*[Aa].*")
        ).map(
                v -> v.toLowerCase()
        ).sorted().collect(Collectors.toList());

        System.out.println(tt);

    }

    private static void metoda8() {

        List<String> lista = Arrays.asList("Ala", "Ela", "testy", "Jacek", "Basia", "Arek");
        List<String> tmp = lista.stream().filter(
                v -> {
                    System.out.println("Sprawdzamy: " + v);
                    if (v.endsWith("a")) {
                        return true;
                    }
                    return false;
                }
        ).map(
                v -> v.toUpperCase()
        ).collect(Collectors.toList());

        System.out.println(tmp);

    }

}
