package day_14;

import sun.util.resources.LocaleData;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Locale;

/**
 * data i czas
 */

public class Zadanie171 {

    public static void main(String[] args) {

//        metoda1();
//        metoda2();
//        metoda3();
//        metoda4();
//        metoda5();
//        metoda6();
//        metoda7();
        metoda8();

    }

    private static void metoda1() {

        LocalDate data = LocalDate.of(2019, Month.JUNE, 2);
        System.out.println("Dzisiejsza data to " + data);
        System.out.println("Dzień roku: " + data.getDayOfYear());
        System.out.println("Jutro będzie: " + data.plusDays(1));

    }

    private static void metoda2() {

        LocalTime time = LocalTime.of(14, 13, 23, 43434);
        System.out.println("Mój czas to " + time);

        System.out.println(time.getMinute());

    }

    private static void metoda3() {

        LocalDateTime datetime = LocalDateTime.of(2019, 4, 23, 3, 33, 33, 33);
        System.out.println(datetime);

        LocalDateTime now = LocalDateTime.of(LocalDate.now(), LocalTime.now());
        System.out.println(now);

        LocalDateTime now2 = LocalDateTime.now();
        System.out.println(now2);


        System.out.println(now.getMonth());

        LocalDateTime now3 = LocalDateTime.now();
        System.out.println(now.withYear(1999).withMonth(12));

    }

    private static void metoda4() {

        LocalDate now = LocalDate.now();
        System.out.println(now);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MMMM yyyy");
        System.out.println(dtf.format(now));

    }

    private static void metoda5() {

        String data = "2019 May 04 23:11:23";
        String wzorzec = "yyyy MMMM dd HH:mm:ss";

        DateTimeFormatter form = DateTimeFormatter.ofPattern(wzorzec);
        LocalDateTime local = LocalDateTime.parse(data, form);
        System.out.println(local);

        DateTimeFormatter form2 = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss");
        System.out.println(form2.format(local));

    }

    private static void metoda6() {

        LocalDate local1 = LocalDate.parse("2019-03-12");
        LocalDate local2 = LocalDate.parse("2020-02-11");

        Period period = Period.between(local1, local2);
        System.out.println(period);
    }

    private static void metoda7() {

        LocalTime local1 = LocalTime.parse("23:29:21");
        LocalTime local2 = LocalTime.parse("23:33:29");

        Duration dur = Duration.between(local1, local2);
        System.out.println(dur.getSeconds() + " sekund lub " + dur.toMinutes() + " minut");

    }

    private static void metoda8() {

        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        calendar.set(Calendar.HOUR, 33);
        System.out.println(calendar.getTime());

    }

}
