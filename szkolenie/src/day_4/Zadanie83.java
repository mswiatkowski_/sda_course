package day_4;

/**
 * Utwórz metodę, która przyjmuje tablicę ciągów znaków. Metoda ma zwrócić
 * napis w formie zdania (z dużą literom na początku i kropką na końcu).
 * > Dla `["ala", "ma", "kota"]` zwróci `"Ala ma kota."`
 * > Dla `["lubię", "kolor", "żółty"]` zwróci `"Lubię kolor żółty."`
 */

public class Zadanie83 {
    public static void main(String[] args) {

        String[] array = {"ala", "ma", "kota", "a", "kot", "ma", "ale"};
        System.out.println(getData(array));

    }

    static String getData(String[] data) {

        String out = data[0].substring(0,1).toUpperCase() + data[0].substring(1);

        for (int i = 1; i < data.length; i++) {

            out += " " + data[i];

        }

        return out + ".";

    }


}
