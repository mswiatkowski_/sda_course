package day_4;

import java.util.Arrays;
import java.util.Random;

/**
 * Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną losowymi
 * liczbami (użyj klasy `Random()`). Rozmiar tablicy ma być parametrem metody.
 */

public class Zadanie64 {
    public static void main(String[] args) {

        System.out.println(Arrays.toString(randomArray(5)));

    }

    static int[] randomArray(int arraySize) {

        int[] newArray = new int[arraySize];
        Random rand = new Random();

        for (int i = 0; i < arraySize; i++) {
            newArray[i] = rand.nextInt(21);
        }

        return newArray;

    }


}
