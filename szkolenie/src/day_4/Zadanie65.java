package day_4;

import java.util.Arrays;
import java.util.Random;

/**
 * Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną losowymi liczbami.
 *
 * Pierwszy - liczba liczb do wylosowania (czyli rozmiar zwracanej tablicy),
 * druga - początek zakresu losowania,
 * trzecia - koniec zakresu losowania.
 *
 * Metoda ma zwrócić tablicę wypełnioną wartościami losowymi.
 *
 */

public class Zadanie65 {
    public static void main(String[] args) {

        System.out.println(Arrays.toString(randomArray(2, 5, 6)));

    }

    static int[] randomArray(int arraySize, int startNum, int endNum) {

        if (endNum < startNum || arraySize < 0) {
            return new int[0];
        }

        int[] out = new int[arraySize];
        Random rand = new Random();

        for (int i = 0; i < out.length; i++) {

            out[i] = rand.nextInt(endNum - startNum + 1) + startNum;

        }

        return out;

    }
}
