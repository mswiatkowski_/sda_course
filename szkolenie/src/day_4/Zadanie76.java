package day_4;

/**
 * Utwórz metodę, która przyjmuje liczbę w formie napisu, a następnie ma zwrócić (jako `String`) sumę wszystkich cyfr
 * > Dla `"536"`, zwróci `"14"` (bo 5 + 3 + 6 = 14)
 * > Dla `"12345"`, zwróci `"15"`
 */

public class Zadanie76 {
    public static void main(String[] args) {

        System.out.println(convertString("123"));

    }

    static String convertString(String value) {

        String[] valuesplit = value.split("");
        int sum = 0;

        for (int i = 0; i < valuesplit.length ; i++){

            sum += Integer.parseInt(valuesplit[i]);

        }

        return String.valueOf(sum);

    }
}
