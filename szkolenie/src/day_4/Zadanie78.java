package day_4;

/**
 * Utwórz metodę, która przyjmuje dwa parametry - pierwszy datą (jako ciąg znaków),
 * a drugi jest separatorem znaków. Metoda ma zwrócić rok z przekazanej daty.
 * > Dla `("12/05/2018", "/")`, zwróci `"2018"`
 * > Dla `("22.11.2007", ".")`, zwróci `"2007"`
 */

public class Zadanie78 {
    public static void main(String[] args) {

        System.out.println(getYear("23/11/2019", "/"));

    }

    static String getYear(String data, String separator) {

        return data.split(separator)[2];

    }
}
