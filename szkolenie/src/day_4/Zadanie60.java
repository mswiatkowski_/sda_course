package day_4;

import java.util.Arrays;

/**
 * Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca nową
 * dwuelementową tablicę zawierającą największą
 * parzystą (na indeksie `0`) i największą nieparzystą (na indeksie `1`)
 * liczbę z podanej tablicy.
 * >dla `[10, 1, 7, 4, 5]`
 * > zwróci `[10, 7]`
 */

public class Zadanie60 {
    public static void main(String[] args) {

        int[] array = {-34, -43, -11};
        System.out.println(Arrays.toString(getData(array)));

    }

    static int[] getData(int[] array) {

        int[] out = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        int indexParz = 0;
        int indexNiep = 1;

        for (int i = 0; i < array.length; i++) {

            if (array[i] % 2 == 0) {

                if (array[i] > out[indexParz]) {
                    out[indexParz] = array[i];
                }

            } else {

                if (array[i] > out[indexNiep]) {
                    out[indexNiep] = array[i];
                }

            }

        }

        return out;

    }

}
