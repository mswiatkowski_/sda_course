package day_4;

import java.util.Arrays;
import java.util.Random;

/**
 * Utwórz metodę, która przyjmuje wysokość trójkąta Pascala
 * a następnie tworzy odpowiednią strukturę danych i ją wypełnia
 * > dla `6`  zwróci
 * >`[1]`
 * >`[1, 1]`
 * >`[1, 2, 1]`
 * >`[1, 3, 3, 1]`
 * >`[1, 5, 10, 10, 5, 1]`
 * >`[1, 6, 15, 20, 15, 6, 1]`
 * <p>
 * Jak kalkulować wartość:
 * ```int number = number * (wiersz - kolumna) / (kolumna + 1);```
 */

public class Zadanie70 {
    public static void main(String[] args) {

        System.out.println(Arrays.deepToString(pascal(5)));

    }

    static int[][] pascal(int high) {

        int[][] out = new int[high][];
        Random rand = new Random();

        for (int i = 0; i < out.length; i++) {

            int number = 1;
            out[i] = new int[i + 1];

            for (int j = 0; j <= i; j++) {
                out[i][j] = number;
                number = number * (i - j) / (j + 1);
            }

        }

        return out;

    }


}
