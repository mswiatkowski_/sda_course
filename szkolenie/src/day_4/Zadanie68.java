package day_4;

import java.util.Arrays;

/**
 * Utwórz metodę, która przyjmuje dwuwymiarową tablicę.
 * Metoda ma zwracać jednowymiarową tablicę która będzie zawierała sumy wszystkich kolumn.
 */

public class Zadanie68 {
    public static void main(String[] args) {

        int[][] ar = {{1, 2, 3, 4}, {2, 3, 4, 5}, {4, 3, 2, 1}, {4, 5, 7, 22}};

        System.out.println(Arrays.toString(sum(ar)));

    }

    static int[] sum(int[][] array) {

        int[] out = new int[array[0].length];

        for (int wiersz = 0; wiersz < array.length; wiersz++) {

            for (int kolumna = 0; kolumna < array[0].length; kolumna++) {

                out[kolumna] += array[wiersz][kolumna];

            }

        }

        return out;

    }
}
