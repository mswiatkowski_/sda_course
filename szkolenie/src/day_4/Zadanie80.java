package day_4;

/**
 * Utwórz metodę, która przyjmuje zdanie (np.
 * `"Ala ma kota."` lub `"Dziś jest sobota"`), a następnie ma
 * zwrócić sumę długości wszystkich wyrazów (bez białych znaków, czyli np. spacji)
 */

public class Zadanie80 {
    public static void main(String[] args) {

        System.out.println(getData("testowe wyrazy"));

    }

    static int getData(String data) {

        String[] tmp = data.split(" ");
        int out = 0;

        for (int i = 0; i < tmp.length; i++) {
            out += tmp[i].length();
        }

        return out;
    }
}
