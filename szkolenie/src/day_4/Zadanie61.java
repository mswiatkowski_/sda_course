package day_4;

/**
 * Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
 * Metoda ma zwrócić *ile elementów* (idąc po kolei od lewej) należy zsumować
 * by przekroczyć podany (jako drugi) parametr
 * > dla `([1,2,3,4,5,6],  9)`
 * > należy zwrócić `4`
 */

public class Zadanie61 {
    public static void main(String[] args) {

        int[] array = {3,5,7,2,1,4};
        System.out.println(getData(array, 7));

    }

    static int getData(int[] array, int value) {

        int sum = 0;
        int counter = 0;

        for (; counter < array.length; counter++) {

            sum += array[counter];

            if (sum > value) {
                return counter + 1;
            }

        }

        return counter;

    }


}
