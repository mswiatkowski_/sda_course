package day_4;

/**
 * Utwórz metodę, która przyjmuje dwa parametry oraz *wyświetla* ciąg elementów.
 * Pierwszy parametr metody określa wyświetlany element, a drugi parametr liczbę wystąpień,
 * > Dla `9, 5` wyświetli: `9 99 999 9999 99999`
 * >
 * > dla `3, 3` wyświetli: `3 33 333`
 * >
 * > dla `8 4` wyświetli: `8 88 888 8888`
 *
 */

public class Zadanie66 {
    public static void main(String[] args) {

        out(4,7);

    }

    static void out(int val1, int val2) {

        for (int i = 0; i < val2; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print(val1);
            }

            System.out.print(" ");

        }

    }
}
