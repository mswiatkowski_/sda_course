package day_4;

/**
 * Utwórz metodę, która przyjmuje imię i nazwisko a następnie zwraca w formie napisu login, który składa się z:
 * >- 3 liter imienia
 * >- 3 liter nazwiska.s
 * >- liczby, która jest sumą długości imienia i nazwiska
 *
 * > Dla `"Maciej Czapla"`, zwróci `"maccza12"`
 * > Dla `"Anna Nowak"`, zwróci `"annnow9"`
 * > Dla `"Jan Kowalski"`, zwróci `"jankow11"`
 */

public class Zadanie77 {
    public static void main(String[] args) {

        System.out.println(username("Michał Świątkowski"));

    }

    static String username(String name) {

        String[] data = name.split(" ");
        String tmp = "";
        int datalenght = 0;

        for (int i = 0; i < data.length; i++) {

            tmp += data[i].substring(0,3);
            datalenght += data[i].length();

        }

        return (tmp + datalenght).toLowerCase();

    }

}
