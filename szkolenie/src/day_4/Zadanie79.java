package day_4;

/**
 * Utwórz metodę, która przyjmuje wyraz, a następnie zwraca środkowy element.
 * W przypadku nieparzystej długości ma zostać zwrócony wyraz długości
 * `1`, a dla parzystych długości `2`
 * > Dla `"Ala"`, zwróci `"l"`
 * > Dla `"Anna"`, zwróci `"nn"`
 */

public class Zadanie79 {
    public static void main(String[] args) {

        System.out.println(getData("texty"));

    }

    static String getData(String text) {

        String[] data = text.split("");
        String out;

        if (data.length % 2 == 0) {
            out = data[(data.length / 2) - 1] + data[(data.length / 2) + 1];
        } else {
            out = data[data.length / 2];
        }

        return out;

    }

}
