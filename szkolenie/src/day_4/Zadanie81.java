package day_4;

/**
 * Utwórz metodę, która przyjmuje ciąg znaków i zwraca informacje czy jest palindromem.
 * > Dla `"kajak"` zwróci `true`
 * > Dla `"samochód"` zwróci `false`
 * > Dla `"a to idiota"` zwróci `true`
 * > Dla `"zaraz będzie ciemno"` zwróci `false`
 * > Dla `"może jutro ta dama da tortu jeżom"` zwróci `true`
 */

public class Zadanie81 {
    public static void main(String[] args) {

        System.out.println(checkData("kajak"));

        System.out.println(checkData2("kajak "));

    }

    static boolean checkData(String data) {

        String[] tmp = data.split("");

        for (int i = 0; i < data.length() / 2; i++) {

            if (!tmp[i].equals(tmp[data.length() - i - 1])) {
                return false;
            }

        }

        return true;

    }

    static boolean checkData2(String data) {

        String str = data.replaceAll(" ", "");
        String text = new StringBuilder(str).reverse().toString();
        return text.equalsIgnoreCase(str);

    }

}
