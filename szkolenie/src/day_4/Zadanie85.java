package day_4;

/**
 * Utwórz metodę, która przyjmuje ciąg znaków (gdzie wyrazy rozdzielone są wielkimi literami)
 * a następnie zwraca napis w którym elementy rozdzielone są znakiem `_` lub znakiem przekazanym jako parametr
 * > Dla `"ZbiorInformacji"` zwróci `"zbior_informacji"`
 * > Dla `"ZieloneSzybkieAuto"` zwróci `"zielone_szybkie_auto"`
 * > Dla `"AlaMaKota"` zwróci `"ala_ma_kota"`
 */

public class Zadanie85 {

    public static void main(String[] args) {

        System.out.println(getData("MichalSwiatkowski", "/"));
        System.out.println(getData("MichalSwiatkowski"));

    }

    static String getData(String data) {

        return getData(data, "_");

    }

    static String getData(String data, String separator) {

        String[] tmp = data.split("");
        String out = tmp[0].toLowerCase();

        for (int i = 1; i < data.length(); i++) {

            if (tmp[i].matches("[A-Z]")) {
                out += separator + tmp[i].toLowerCase();
            } else {
                out += tmp[i];
            }

        }

        return out;

    }


}
