package day_4;

import java.util.Arrays;

/**
 * Utwórz metodę, która przyjmuje tablicę liczb i zwraca nową tablicę w której wszystkie ujemne liczby będą
 * w lewej części, a wszystkie dodanie będą w prawej części. Liczby ujemne i dodatnie mogą
 * być w dowolnej kolejności (tzn. `1,2,3` lub `3,2,1` są jak najbardziej poprawne)
 * > Dla `[1, 2, -6, -9, 11, 0, -2]`
 * > zwróci `[-6, -9, -2, 0, 11, 2, 1]`.
 */

public class Zadanie63 {
    public static void main(String[] args) {

        int[] array = {3, 6, 3, -1, 4, 0, 6, -7, 4, 2, -3};
        System.out.println(Arrays.toString(sortArray(array)));

    }

    static int[] sortArray(int[] array) {

        int[] newArray = new int[array.length];

        int leftINdex = 0;
        int righrIndex = array.length - 1;

        for (int i = 0; i < array.length; i++) {

            if (array[i] < 0) {
                newArray[leftINdex] = array[i];
                leftINdex += 1;
            }

            if (array[i] > 0) {
                newArray[righrIndex] = array[i];
                righrIndex -= 1;
            }

        }

        return newArray;

    }


}
