package day_4;

import java.util.Arrays;
import java.util.Random;

/**
 * Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy,
 * a następnie zwraca nowo utworzoną, *dwuwymiarową* tablicę wypełnioną losowymi
 * wartościami. Utwórz drugą metodę do wyświetlania zwróconej tablicy.
 */

public class Zadanie67 {
    public static void main(String[] args) {

        printArray(twoDImensionsArray(5, 5));

    }

    static int[][] twoDImensionsArray(int length, int width) {

        Random rand = new Random();
        int[][] out = new int[length][width];

        for (int i = 0; i < length; i++) {

            for (int j = 0; j < width; j++) {

                out[i][j] = rand.nextInt(21);

            }

        }

        return out;

    }

    static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
