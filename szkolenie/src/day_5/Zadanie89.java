package day_5;

/**
 * Porównaj czas sumowanie elementów przy użyciu klas:
 * - `String`
 * - `StringBuilder()`
 * - `StringBuffer()`
 */

public class Zadanie89 {
    public static void main(String[] args) {

        int value = 1_000_000;

        long start = System.currentTimeMillis();
//        testString1(value);
        long end = System.currentTimeMillis();
        System.out.println("String: " + (end - start));

        start = System.currentTimeMillis();
        testString2(value);
        end = System.currentTimeMillis();
        System.out.println("Builder: " + (end - start));

        start = System.currentTimeMillis();
        testString3(value);
        end = System.currentTimeMillis();
        System.out.println("Buffer: " + (end - start));

    }

    static void testString1(int counter) {

        String text = "";

        for (int i = 0; i < counter; i++) {
            text += "q";
        }

    }

    static void testString2(int counter) {

        StringBuilder text = new StringBuilder();

        for (int i = 0; i < counter; i++) {
            text.append("q");
        }

    }

    static void testString3(int counter) {

        StringBuffer text = new StringBuffer();

        for (int i = 0; i < counter; i++) {
            text.append("q");
        }

    }

}
