package day_5;

/**
 * Utwórz metodę, która przyjmuje conajmniej DWA parametry - boolean oraz dowolną ilość
 * (większą od 0) liczb (typu `int`). Gdy pierwszy parametr ma wartość `true`
 * metoda zwraca największą przekazaną liczbą, a gdy `false` najmniejszą.
 */

public class Zadanie92 {
    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, -4};

        System.out.println(minMax(false, -8, array));
        System.out.println(minMax(true, 89, array));

    }

    static int minMax(boolean checkMinMax, int firstVal, int... array) {

        int out = firstVal;

        for (int i = 1; i < array.length; i++) {
            if (checkMinMax && array[i] > out) {
                out = array[i];
            }

            if (!checkMinMax && array[i] < out) {
                out = array[i];
            }
        }

        return out;

    }
}
