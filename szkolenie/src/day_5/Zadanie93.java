package day_5;

import javax.print.DocFlavor;

/**
 * REGEX - `imię (opcjonalne drugie imię) nazwisko`
 */

public class Zadanie93 {
    public static void main(String[] args) {

        System.out.println(name("Michal Michal Swiatkowski"));

    }

    static String name(String name) {

        if (name.matches("[A-Za-z]+ ([A-Za-z]+ )?[A-Za-z]+")) {
            return "Imie poprawne";
        } else {
            return "Imie niepoprawne";
        }
    }

}
