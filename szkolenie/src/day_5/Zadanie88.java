package day_5;

/**
 * Utwórz metodę, która przyjmuje ciąg znaków, a odwraca każdy z wyrazów i zwraca całość jako jeden napis.
 * > Dla `"ala ma kota"` zwróci`“ala am atok”`
 * > Dla `"dzisiaj jest sobota"` zwróci`“jaisizd tsej atobos”`
 */

public class Zadanie88 {
    public static void main(String[] args) {

        System.out.println(text("Ala ma kota"));

    }

    static String text(String text) {

        String[] t = text.split(" ");
        String out = "";

        for (int i = 0; i < t.length; i++) {

            out += reverse(t[i]) + " ";

        }

        return out;

    }

    static String reverse(String text) {

        String[] tmp = text.split("");
        String out = "";

        for (int j = tmp.length - 1; j >= 0 ; j--) {
            out += tmp[j];
        }

        return out;

    }
}
