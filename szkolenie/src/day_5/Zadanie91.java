package day_5;

import javax.print.DocFlavor;

/**
 * Utwórz metodę, która wykorzystuje *varargs* by przekazać do metody dowolną,
 * większą od zera, liczbę elementów typu `String` i zwrócić jeden napis sklejony z nich.
 */

public class Zadanie91 {
    public static void main(String[] args) {
        System.out.println(test("Ala", "ma", "kota"));
    }

    static String test(String first, String... text) {

        StringBuilder out = new StringBuilder(first);

        for (int i = 0; i < text.length; i++) {
            out.append(" ").append(text[i]);
        }

        return out.toString();

    }
}
