package day_5;

/**
 * Utwórz metodę, która wykorzystuje mechanizm *varargs* by przekazać do metody
 * dowolną, większą od zera, liczbę elementów typu `int` i zwrócić ich sumę.
 */

public class Zadanie90 {
    public static void main(String[] args) {

        int[] array = {2, 3, 4, 5, 7, 4, 2, 4, 6};

        System.out.println(suma(1, 2, 3, 4, 5, 6));
        System.out.println(suma(1, array));
        System.out.println(suma(1));

    }

    /*
        pierwszy parametr po to aby wymusić podanie przynajmniej jednego argumentu
        int ... tab - samo w sobie nie wymusza podania argumentu, dla braku argumentów zwróci 0
     */
    static int suma(int var, int... tab) {
        int suma = var;

        for (int i = 0; i < tab.length; i++) {
            suma += tab[i];
        }
        return suma;
    }

}
