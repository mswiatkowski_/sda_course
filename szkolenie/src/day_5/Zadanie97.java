package day_5;

/**
 * REGEX - `adres url`
 */

public class Zadanie97 {
    public static void main(String[] args) {

        String regex = "https?://(www.)?([a-z0-9]+\\.)+[a-z]+";
        System.out.println("https://test.testy.stronawww.pl".matches(regex));

    }
}
