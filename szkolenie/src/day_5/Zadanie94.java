package day_5;

/**
 * REGEX - `numer telefonu (wersja z myślnikami oraz bez)`
 */

public class Zadanie94 {

    public static void main(String[] args) {

        String regex = "[0-9]{9}|[0-9]{3}-[0-9]{3}-[0-9]{3}";
        System.out.println("506-421-134".matches(regex));

    }

}
