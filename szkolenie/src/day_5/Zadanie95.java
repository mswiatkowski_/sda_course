package day_5;

/**
 * REGEX - `adres IP`
 */

public class Zadanie95 {

    public static void main(String[] args) {

        String regex = "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
        System.out.println("127.0.0.111".matches(regex));

    }
}
