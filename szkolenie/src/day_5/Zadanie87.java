package day_5;

import java.util.Arrays;

/**
 * Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma “przesunąć”
 * elementy w środku tablicy o tyle ile wynosi wartość drugiego elementu i zwrócić nową tablicę.
 * Dla wartości dodatniej przesuwa w prawo, a dla ujemnej w lewo. Elementy z końca tablicy lądują
 * na początku (i na odwrót)
 * > dla `([1,2,3,4,5,6],  2)`
 * > należy zwrócić `[5,6,1,2,3,4]` (przesunięcie o dwa w prawo)
 * >
 * > dla `([1,2,3,4,5,6],  -3)`
 * > należy zwrócić `[4,5,6,1,2,3` (przesunięcie o trzy w lewo)
 */

public class Zadanie87 {
    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5, 6};

        System.out.println(Arrays.toString(test(array, -1)));
        System.out.println(Arrays.toString(test(array, 1)));
        System.out.println(Arrays.toString(test(array, 0)));
    }

    static int[] test(int[] array, int val) {

        int length = array.length;
        int[] out = new int[length];
        int value = 0;

        if (val == 0) {
            return array;
        }

        if (val < 0) {
            value = length - Math.abs(val);
        } else {
            value = val;
        }

        for (int i = 0; i < array.length - value; i++) {
            out[i + value] = array[i];
        }

        for (int i = 0; i < value; i++) {
            out[i] = array[length - value + i];
        }

        return out;

    }

}
