package day_5;

/**
 * REGEX - `adres email`
 */

public class Zadanie96 {

    public static void main(String[] args) {

        String regex = "[a-zA-Z0-9][a-zA-Z0-9\\._\\-\\+]+@.+\\.[a-z]+";
        System.out.println("michal@michal.pl".matches(regex));

    }

}
