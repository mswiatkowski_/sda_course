package day_3;

public class Zadanie47 {

    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5, 6, 8, 9};
        System.out.println(inarray(array, 7));

    }

    /**
     *
     * Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy.
     *
     */

    static boolean inarray(int[] array, int value) {

        for (int i = 0; i < array.length; i++) {
            if(array[i] == value) {

                return true;

            }
        }

        return false;

    }
}
