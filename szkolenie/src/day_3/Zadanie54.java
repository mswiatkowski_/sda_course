package day_3;

import java.util.Arrays;

public class Zadanie54 {
    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(changeArray(array, 4, 33)));

    }

    /**
     *
     * Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby. Metoda ma zwrócić nową tablicę do której
     * na wybranej pozycji (podanej jako drugi parametr) wstawi nowy element (podany jako trzeci parametr).
     * > Dla `([1, 2, 3, 4, 5], 2, 77)`
     * > powinno zwrócić `[1, 2, 77, 3, 4, 5]`
     *
     * zrealizuj przy użyciu TYLKO JEDNEJ pętli.
     *
     * todo : index poza zakresem
     *
     */

    static int[] changeArray(int[] array, int index, int value) {

        int[] newArray = new int[array.length + 1];
        int newIndex = 0;

        for (int i = 0; i <= array.length; i++) {

            newIndex = i;

            if (i == index && index < array.length) {
                newArray[i] = value;
                newIndex += 1;
            }

            if (i > index) {
                newArray[newIndex] = array[i - 1];
            } else {
                newArray[newIndex] = array[i];
            }

        }

        return newArray;

    }
}
