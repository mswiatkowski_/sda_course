package day_3;

import java.util.Arrays;

public class Zadanie51 {
    public static void main(String[] args) {

        int[] array = {2,4,6,8,10,12,13,14,15,16,19};

        System.out.println(Arrays.toString(arrayshift(array)));
        System.out.println(Arrays.toString(arrayshift2(array)));

    }

    /**
     *
     * Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
     *
     */

    static int[] arrayshift(int[] array) {

        int length = array.length;
        int[] ar = new int[length];

        for (int i = length - 1; i >= 0; i--) {

            ar[i] = array[length - 1 - i];

        }

        return ar;

    }

    static int[] arrayshift2(int[] array) {

        for (int i = 0; i < array.length / 2; i++) {

            int newidex = array.length - 1 - i;
            int tmp = array[i];
            array[i] = array[newidex];

            array[newidex] = tmp;

        }

        return array;

    }

}
