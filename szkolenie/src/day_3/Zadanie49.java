package day_3;

public class Zadanie49 {
    public static void main(String[] args) {

        printArray(arrayprint(6));

    }

    /**
     *
     * Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
     * > Dla `4` zwróci: `[10, 11, 12, 13]`
     * >
     * > Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
     *
     */

    static int[] arrayprint(int length) {

        int[] array = new int[length];

        for (int i = 0; i < length; i++) {
            array[i] = 10 + i;
        }

        return array;

    }

    static void printArray(int[] array) {

        for (int i = 0; i < array.length; i++) {

            System.out.print(array[i] + " ");

        }

    }

}
