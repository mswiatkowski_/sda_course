package day_3;

public class Zadanie46 {
    public static void main(String[] args) {

        int[] tab2 = {1, 2, 4, 5, 6, 7, 8, 9};
        System.out.println(sum(tab2));

    }

    /**
     *
     * Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę wszystkich elementów
     *
     */

    static int sum(int[] table) {

        int test = 0;

        for (int i = 0; i < table.length; i++) {

            test += table[i];

        }

        return test;

    }
}
