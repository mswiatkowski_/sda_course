package day_3;

import java.util.Arrays;

public class Zadanie53 {
    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(changedArr(array, 6, 33)));

    }

    /**
     * Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby. Metoda ma zwrócić nową tablicę do której
     * na wybranej pozycji (podanej jako drugi parametr) wstawi nowy element (podany jako trzeci parametr).
     * > Dla `([1, 2, 3, 4, 5], 2, 77)`
     * > powinno zwrócić `[1, 2, 77, 3, 4, 5]`
     * <p>
     * 2 petle / do indexu i po indexie / ogarnac przypadek kiedy index jest spoza zakresu
     */

    static int[] changedArr(int[] array, int index, int value) {

        int[] biggerArray = new int[array.length + 1];
        int newIndex = Math.min(index, array.length);
        for (int i = 0; i < newIndex; i++) {
            biggerArray[i] = array[i];

        }
        if (array.length >= index) {

            biggerArray[index] = value;
        } else {
            biggerArray[newIndex] = value;
        }

        for (int i = index; i < array.length; i++) {
            biggerArray[i + 1] = array[i];
        }

        return biggerArray;

    }

}
