package day_3;

public class Zadanie59 {
    public static void main(String[] args) {


    }

    /**
     *
     * Utwórz metodę, która przyjmuje dwie tablice. Metoda ma zwrócić tablicę z sumą kolumn podanych tablic. To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
     * *Przyjęte założenia:*
     * - Tablice mogą być różnych długości!
     * - Liczby sumujemy jak w “dodawaniu pisemnym”, tzn. gdy wartość jest większa od `9` to `1` “idzie dalej”.
     *
     * >
     * ```[1,  2,  3,  4]
     *    [ 6,  7,  8]
     * [1,  9,  1,  2]```
     *
     */
}
