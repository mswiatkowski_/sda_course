package day_3;

public class Zadanie48 {
    public static void main(String[] args) {

        int[] array = {-1, -33, -5, -7, -9};
        int[] array2 = {1, 33, 5, 7, 9};
        int[] array3 = {};
        int[] array4 = {6};
        System.out.println(maxval(array));
        System.out.println(maxval(array2));
        System.out.println(maxval(array3));
        System.out.println(maxval(array4));

    }

    /**
     *
     * Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca *NAJWIĘKSZĄ* liczbę z tej tablicy
     */

    static int maxval(int[] array) {

        if(array.length == 0) {
            return 0;
        }

        int max = array[0];

        for (int i = 1; i < array.length; i++) {

            if(array[i] > max) {
                max = array[i];
            }

        }

        return max;

    }

}
