package day_3;

public class Zadanie52 {
    public static void main(String[] args) {

//        int[] array = {200, 200, 33, 40, 50, 45};
        int[] array = {};
        System.out.println(secondBigNum(array));

    }

    /**
     * Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
     * > dla `[1, 2, 3, 4, 5]` zwróci `4`
     */

    static int secondBigNum(int[] array) {

        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {

            int aval = array[i];

            if (aval > max1) {
                max2 = max1;
                max1 = aval;
            }

            if (aval > max2 && aval < max1) {
                max2 = aval;
            }

        }

        return max2;

    }

}
