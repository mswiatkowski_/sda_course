package day_3;

public class Zadanie44 {

    public static void main(String[] args) {

        System.out.println(numlenght("Ala ma kota"));

    }

    /**
     *
     * Utwórz metodę, która przyjmuje zdanie (np. `"Ala ma kota."` lub `"Dziś jest sobota"`), a następnie ma zwrócić sumę długości wszystkich wyrazów (bez białych znaków, czyli np. spacji)
     *
     */

    static int numlenght(String value) {

        return value.replaceAll(" ", "").length();

    }
}
