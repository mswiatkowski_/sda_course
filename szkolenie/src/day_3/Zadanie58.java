package day_3;

import java.util.Arrays;

public class Zadanie58 {
    public static void main(String[] args) {

        int[] array1 = {1,2,4,6,3,1,4,6,7,4,2,4,6,7,3,2};
        int[] array2 = {1,2,4,6,3,1,4,6,7,4,2,4,6,7,3,2};

        System.out.println(Arrays.toString(sum(array1, array2)));

    }

    /**
     *
     * Utwórz metodę, która przyjmuje dwie tablice. Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
     * To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
     * *Przyjęte założenia:*
     * - Tablice są tych samych długości.
     * - Wartości na danej pozycji mogą być większę niż 10
     *
     * >
     * [1,  2,  3,  4]
     * [5,  6,  7,  8]
     * [6,  8, 10, 11]```
     *
     */

    static int[] sum(int[] firstArray, int[] secondArray) {

        int[] sumArray = new int[firstArray.length];

        for (int i = 0; i < firstArray.length; i++) {
            sumArray[i] = firstArray[i] + secondArray[i];
        }

        return sumArray;

    }

}
