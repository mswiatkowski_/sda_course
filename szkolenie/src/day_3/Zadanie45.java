package day_3;

public class Zadanie45 {
    public static void main(String[] args) {

        System.out.println(text("ala ma kota"));
        text2("lotto");

    }

    /**
     *
     * Utwórz metodę, która przyjmuje wyraz, a następnie zwraca informacje czy zawiera on samogłoski czy nie.
     *
     */

    static boolean text(String value) {

        return (value.contains("a") ||
                value.contains("e") ||
                value.contains("i") ||
                value.contains("o") ||
                value.contains("u") ||
                value.contains("y"));

    }

    static void text2(String value) {

        char[] chars = new char[6];
        chars[0] = 'a';
        chars[1] = 'e';
        chars[2] = 'i';
        chars[3] = 'o';
        chars[4] = 'u';
        chars[5] = 'y';

        for (int i = 0; i < chars.length; i++) {

            System.out.println(chars[i]);
            
        }

    }

}
