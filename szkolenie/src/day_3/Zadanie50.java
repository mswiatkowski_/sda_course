package day_3;

public class Zadanie50 {
    public static void main(String[] args) {

        int[] array = {2, 4, 6, 7, 3, 4, 6, 7};
        System.out.println(index(array, 11));
        System.out.println(index(array, 3));

    }

    /**
     *
     * Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma zwrócić indeks szukanego elementu.
     * Gdy dana liczba nie będzie znajdować się w tablicy, metoda powinna zwrócić wartość `-1`.
     *
     */

    static int index(int[] array, int value) {

        for (int i = 0; i < array.length; i++) {

            if (array[i] == value) {
                return i;
            }

        }

        return -1;

    }
}
