package day_3;

import java.util.Arrays;

public class Zadanie56 {
    public static void main(String[] args) {

        int[] array = {1,2,4,6,3,1,4,6,7,4,2,4,6,7,3,2};
        System.out.println(Arrays.toString(delFromArray(array, 2)));

    }

    /**
     *
     * Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę. Metoda ma usunąć wszystkie elementy równe
     * podanemu parametrowi (tzn. gdy podamy `2` to ma usunąć z tablicy wszystkie `2`-jki)
     * > Dla `([1, 2, 2, 4, 5],  2)`
     * > zwróci `[1, 4, 5]`
     *
     */

    static int[] delFromArray(int[] array, int value) {

        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                counter++;
            }
        }

        if (counter == 0) {
            return  array;
        }

        int delCount = 0;
        int[] newArray = new int[array.length - counter];

        for (int i = 0; i < array.length; i++) {

            if (array[i] == value) {
                delCount += 1;
            } else {
                newArray[i - delCount] = array[i];
            }

        }

        return newArray;

    }
}
