package zad161;

import java.io.Serializable;

/**
 * *ZADANIE #161 - SERIALIZACJA*
 *
 * Utwórz klasę `zad161.Wiadomosc` z polami `tresc` (typ `String`), `autor`
 * (typ `String`) oraz zapisz serializowany obiekt do pliku.
 */

public class Wiadomosc implements Serializable {

    private String tresc;
    private transient String autor;     // transient powoduje ze pole nie jest serializowane

    public Wiadomosc(String tresc, String autor) {
        this.tresc = tresc;
        this.autor = autor;
    }

    public String getAutor() {
        return autor;
    }

    public String getTresc() {
        return tresc;
    }



}
