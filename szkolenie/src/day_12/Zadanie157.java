package day_12;

/*
*ZADANIE #157*
Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku, a następnie wyświetla:
- liczbę linii w pliku
- liczbę wyrazów w pliku
- liczbę znaków w pliku
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Zadanie157 {

    public static void main(String[] args) {

        try {
            List<String> lista = printFile("files/zadanie156");
            System.out.println(ileLinii(lista));
            System.out.println(ileWyrazow(lista));
            System.out.println(ileWZnakow(lista));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static List<String> printFile(String path) throws IOException {

        FileReader file = new FileReader(path);
        BufferedReader reader = new BufferedReader(file);

        String linia = reader.readLine();

        List<String> pliczek = new ArrayList<>();

        while (linia != null) {

            pliczek.add(linia);
            linia = reader.readLine();

        }

        return pliczek;

    }

    static int ileLinii(List<String> input) {
        return input.size();
    }

    static int ileWyrazow(List<String> input) {
        int sum = 0;

        for (String s : input) {
            sum = s.split(" ").length;
        }

        return sum;
    }

    static int ileWZnakow(List<String> input) {
        int sum = 0;

        for (String s : input) {
            sum = s.split("").length;
        }

        return sum;
    }

}
