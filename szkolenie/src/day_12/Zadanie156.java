package day_12;

/*
 * ZADANIE #156*
 * Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
 * a następnie wyświetla zawartość tego pliku na konsolę.
 * Zrealizuj zadanie odczytując dane “znak po znaku” oraz “linia po linii”
 */

import javax.print.DocFlavor;
import java.io.*;

public class Zadanie156 {

    public static void main(String[] args) {

        try {
            printFile("files/zadanie156");
            printFile2("files/zadanie156");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static void printFile(String path) throws IOException {

        FileReader file = new FileReader(path);
        BufferedReader reader = new BufferedReader(file);

        String linia = reader.readLine();
        while (linia != null) {

            System.out.println(linia);
            linia = reader.readLine();

        }

    }

    static void printFile2(String path) throws IOException {

        FileInputStream file = new FileInputStream(path);
        int number = file.read();

        while (number != -1) {
            System.out.println((char) number);
            number = file.read();
        }

    }

}
