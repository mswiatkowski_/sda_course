package day_12;

/*
*ZADANIE #152*
Utwórz metodę która przyjmuje niepustą liste lub tablicę a następnie w
* nieskończonej pętli je wyświetla (zaczynając od pozycji `0`).
* Pętla ma zostać przerwana po wyjściu poza zakres listy (tzn. po przekroczeniu jej rozmiaru).
 */

import java.util.Arrays;
import java.util.List;

public class Zadanie152 {
    public static void main(String[] args) {

        List<String> l = Arrays.asList("3", "lotto", "4", "testy");
        lista(l);

    }

    static void lista(List<String> list) {

        for (int i = 0; ;i++) {

            try {

                System.out.println(list.get(i));

            } catch (ArrayIndexOutOfBoundsException e) {

                break;

            }

        }

    }
}
