package day_12;

/*
*ZADANIE #151*
*ZADANIE #151*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu
* podania wartości `-1` i *zwraca* ich sumę. W przypadku podania
* złej wartości (np. `aaa`) metoda ma NIE przerywać odczytywania tylko wyświetlić komunikat.
 */

import java.util.Scanner;

public class Zadanie151 {

    public static void main(String[] args) {

        System.out.println(testy());

    }

    static Integer testy() {

        Scanner scanner = new Scanner(System.in);
        Integer out = 0;

        for (; ; ) {

            System.out.print("Podaj liczbę: ");

            try {
                Integer myValue = scanner.nextInt();

                if (myValue == -1) {
                    break;
                } else {
                    out += myValue;
                }

                System.out.println();

            } catch (java.util.InputMismatchException e) {
                System.out.println("Niepoprawna wartość");
                e.printStackTrace();
//                scanner.nextLine();
            }

        }

        return out;

    }

}
