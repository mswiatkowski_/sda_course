package day_12;

/*
*ZADANIE #153*
Utwórz metodę która wykonuję operację dzielenia dwóch liczb
* przekazywanych jako parametry. W przypadku podzielenia przez `0` metoda powinna zwrócić nasz własny wyjątek
 */

public class Zadanie153 {

    public static void main(String[] args) {
        try {
            dzielenie(3, 6);
            dzielenie(5, 0);
        } catch (myException e) {
            e.printStackTrace();
        }

        dzielenie2(3, 5);
        dzielenie2(4.0, 0);

    }

    static void dzielenie(Integer num1, Integer num2) throws myException {

        try {
            System.out.println(num1 / num2);
        } catch (ArithmeticException e) {
            throw new myException();
        }

    }

    static void dzielenie2(double num1, double num2) {

        double num = (num1 / num2);

        if (num == Double.NEGATIVE_INFINITY || num == Double.POSITIVE_INFINITY) {
            throw new testException();
        } else {
            System.out.println(num);
        }

    }

}
