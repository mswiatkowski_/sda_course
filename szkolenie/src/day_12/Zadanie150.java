package day_12;

/*
Maciej Czapla - TRENER [11:11 AM]
*ZADANIE #150 - EXTRA*
Utwórz metodę, która przyjmuje mapę w postaci `String : Integer`. Metoda ma zwrócić mapę w postaci
`Integer : String`, czyli ma zamienić klucze z wartościami. W przypadku gdy podany klucz
będzie już istniał w mapie, metoda ma za zadanie skleić napis ze sobą (rozdzielając je spacjami). (edited)
 */

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Zadanie150 {

    public static void main(String[] args) {

        Map<String, Integer> dane = new HashMap<>();
        dane.put("michal1", 11);
        dane.put("michal2", 8);
        dane.put("michal3", 7);
        dane.put("michal4", 6);
        dane.put("michal5", 4);
        dane.put("michal6", 5);
        dane.put("michal7", 1);
        dane.put("michal8", 9);
        dane.put("michal9", 8);
        dane.put("michal10", 5);
        dane.put("michal11", 3);
        dane.put("michal12", 5);

        System.out.println(mapReverse(dane));

    }

    private static Map<Integer, String> mapReverse(Map<String, Integer> data) {

        Map<Integer, String> out = new TreeMap<>();

        for (String s : data.keySet()) {

            String tmp = out.get(data.get(s));

            if (out.containsKey(data.get(s))) {
                out.put(data.get(s), tmp + "-" + s);
            } else {
                out.put(data.get(s), s);
            }

        }

        return out;

    }

}
