package day_12;

/*
*ZADANIE #154*
Utwórz metodę, która przyjmuje parametry typu `int` korzystając z mechanizmu `varargs`.
* W przypadku nie przekazania żadnego parametru, metoda powinna rzucić
* własny wyjątek (z komunikatem o powodzie). W przypadku podania parametrów, metoda powinna zwrócić ich sumę.
 */
public class Zadanie154 {

    public static void main(String[] args) {

        System.out.println(sum(1, 2, 3, 4, 5, 6, 7, 8));
        System.out.println(sum());

    }

    private static int sum(int... numbers) {

        if (numbers.length == 0) {
            throw new newException();
        }

        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }

        return sum;

    }

}

class newException extends RuntimeException {

    newException() {
        super("Coś nie dziala");
    }

}

