package day_12.Zadanie148;

/*
*ZADANIE #148*
Utwórz klasę Samochod (zawierająca pola `nazwa`, `kolor` oraz *unikalny* `nrRejestracji`),
*  następnie dodaj kilka obiektów do mapy (gdzie kluczem będzie `identyfikator` a wartością *obiekt* klasy `Samochod`).
 */

import java.util.ArrayList;
import java.util.List;

public class Samochod {

    private String nazwa;
    private Kolor kolor;
    private String nrRejestracji;

    private static List<String> rejestracje = new ArrayList<>();

    private Samochod(String nazwa, Kolor kolor, String nrRejestracji) {
        this.nazwa = nazwa;
        this.kolor = kolor;
        this.nrRejestracji = nrRejestracji;

        rejestracje.add(nrRejestracji);
    }

    public static Samochod createCar(String nazwa, Kolor kolor, String nrRejestracji) {

        if (rejestracje.contains(nrRejestracji)) {
            return null;
        } else {
            return new Samochod(nazwa, kolor, nrRejestracji);
        }

    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nazwa='" + nazwa + '\'' +
                ", kolor=" + kolor +
                ", nrRejestracji='" + nrRejestracji + '\'' +
                '}';
    }

    public Kolor getKolor() {
        return kolor;
    }

    public String getNazwa() {
        return nazwa;
    }

    public String getNrRejestracji() {
        return nrRejestracji;
    }

}