package day_12.Zadanie148;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Samochod car1 = Samochod.createCar("Toyota", Kolor.CZERWONY, "EL 1234");
        Samochod car2 = Samochod.createCar("Fiat", Kolor.ZIELONY, "CW 2345");
        Samochod car3 = Samochod.createCar("Opel", Kolor.CZARNY, "PW 5678");

        Samochod car4 = Samochod.createCar("Renault", Kolor.ROZOWY, "EL 1234");

        Map<String, Samochod> listaSamochodow = new HashMap<>();
        listaSamochodow.put(car1.getNrRejestracji(), car1);
        listaSamochodow.put(car2.getNrRejestracji(), car2);
        listaSamochodow.put(car3.getNrRejestracji(), car3);
//        listaSamochodow.put(car4.getNrRejestracji(), car4);

        System.out.print("Podaj szukany nr rejestracyjny: ");

        Scanner scanner = new Scanner(System.in);
        String rej = scanner.nextLine();

        Samochod szukanySamochod = listaSamochodow.get(rej);

        if (szukanySamochod == null) {
            System.out.println("Nie znaleziono pojazdu");
        } else {
            System.out.println(szukanySamochod);
        }

        System.out.println(listaSamochodow);

    }



}
