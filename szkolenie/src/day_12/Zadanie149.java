package day_12;

/*
*ZADANIE #149*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania
* wartości `-1` a następnie zwraca mapę wartości, gdzie kluczem jest liczba a wartością jest liczba jej wystąpień.
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zadanie149 {

    public static void main(String[] args) {

        System.out.println(testy());;

    }

    static Map<Integer, Integer> testy() {

        Scanner scanner = new Scanner(System.in);
        Map<Integer, Integer> myMap = new HashMap<>();

        for (;;) {
            System.out.print("Podaj liczbę: ");
            Integer myValue = scanner.nextInt();
            if (myValue == -1) {
                break;
            }

            if (myMap.containsKey(myValue)) {
                Integer tmp = myMap.get(myValue);
                myMap.put(myValue, tmp + 1);
            } else {
                myMap.put(myValue, 1);
            }



            System.out.println();
        }

        return myMap;

    }

}
