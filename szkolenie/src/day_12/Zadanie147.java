package day_12;

/*
*ZADANIE #147*
Utwórz metodę, która przyjmuje jeden parametr - maksymalną liczbę.
* Metoda powinna zwrócić mapę, gdzie kluczami będą kolejne liczby
* od `1` do podanej liczby (jako parametr). Wartościami tej mapy będzie
* informacja, przez jakie liczby jest ona podzielna.
>
```...
10 -> 1, 2, 5, 10
11 -> 1, 11
12 -> 1, 2, 3, 4, 6, 12
13 -> 1, 13
14 -> 1, 2, 7, 14
...```
 */

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Zadanie147 {

    public static void main(String[] args) {

        System.out.println(dzielniki(11));

    }

    static Map<Integer, Set<Integer>> dzielniki(Integer max) {

        Map<Integer, Set<Integer>> resultMap = new TreeMap<>();

        for (int i = 1; i <= max; i++) {
            resultMap.put(i, pobierzDzielniki(i));
        }

        return resultMap;

    }

    private static Set<Integer> pobierzDzielniki(int number) {

        Set<Integer> result = new TreeSet<>();
        for (int i = 1; i <= number / 2; i++) {

            if (number % i == 0) {
                result.add(i);
            }

        }

        result.add(number);

        return result;

    }

}
