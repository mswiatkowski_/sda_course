package day_12;

/*
*ZADANIE #155*
Utwórz metodę, która przyjmuje dwa parametry - napis (`String`) oraz liczbę (`int`)
* wcześniej odczytaną `Scanner`-em od użytkownika. Metoda ma wyświetlić podany napis,
* przekazaną liczbę razy. W przypadku gdy liczba wystąpień będzie mniejsza bądź równa
* zero powinien zostać rzucony własny wyjątek (dziedziczący po `RuntimeException`).
 */

import java.util.Scanner;

public class Zadanie155 {

    public static void main(String[] args) {

        printText();

    }

    static void printText() {

        Scanner scanner = new Scanner(System.in);

        String out = "";

        int number = scanner.nextInt();
        String text = scanner.next();

        if (number < 1) {
            throw new MyException();
        }

        for (int i = 0; i < number; i++) {
            out = out + text + " ";
        }

        System.out.println(out);

    }

}

class MyException extends RuntimeException {

    MyException() {
        super("Coś nie działą ...");
    }

}
