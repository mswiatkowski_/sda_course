package day_12.Zadanie159;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        ManageData data = new ManageData("files/passwd.txt");

        System.out.println(String.format("Uzytkownicy z takim samych hasłem i loginem: %.2f%%", data.loginPassEquals()));

        System.out.println(String.format("Wyciekło %s użytkowników", data.dataLeak()));

        System.out.println(String.format("Liczba uzytkowników bez cyfry w haśle: %s", data.numberInPass()));

        System.out.println(String.format("Procent uzytkowników bez dużej litery w haśle: %.2f%%", data.bigDataInPass()));

        System.out.println(String.format("Ilu użytkowników ma hasło krótsze niż 6 znaków: %s", data.passwordShorterThan(6)));

        System.out.println(data.passwordsLength());

        System.out.println(data.popularPasswords());
        System.out.println(data.popularPasswords2());

        System.out.println(data.mailBoxCount());
    }

}
