package day_12.Zadanie159;

public class User {

    private String username;
    private String password;

    User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    boolean userEqualsPassword() {
        return username.equals(password);
    }

}
