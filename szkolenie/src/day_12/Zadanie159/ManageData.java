package day_12.Zadanie159;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * *ZADANIE #159*
 *
 * W załączniku jest plik z loginami i hasłami. Waszym zadaniem jest zrobienie analizy tego pliku oraz napisanie metod które *ZWRÓCĄ*:
 *
 * - `[double]` informację jaki % osób miało taki sam login jak hasło (np. `anka40:anka40`)
 *
 * - `[int]` informację ilu użytkowników “wyciekło”
 *
 * - `[int]` informację ilu użytkowników NIE miało cyfry w haśle
 *
 * - `[double]` informację jaki % użytkowników nie używało dużych liter w haśle
 *
 * - `[int]` informację ile użytkowników miało hasło krótsze niż 5 znaków (sprawdzana długość hasła powinna być parameterem metody)
 *
 * - `[Map<Integer, Integer>]` informację jaki był rozkład długości haseł (ile razy wystapiła długość 1, ile 2, ile 3...)
 *
 * - `[List<String>]` listę 10 najpopularniejszych haseł
 *
 * - `[Map<String, Integer>]`  ile razy wystąpiła każda ze skrzynek mailowych (oczywiście jeśli loginem był mail) (edited)
 */

class ManageData {

    private List<User> data = new ArrayList<>();
    private String path;

    ManageData(String path) throws IOException {
        this.path = path;

        this.addData();
    }

    private void addData() throws IOException {

        FileReader file = new FileReader(this.path);
        BufferedReader reader = new BufferedReader(file);

        String linia = reader.readLine();

        while (linia != null) {

            if (!linia.equals("")) {
                String[] tmp = linia.split(":");
                data.add(new User(tmp[0], tmp[1]));
                linia = reader.readLine();
            }

        }

    }

    double loginPassEquals() {

        double equals = 0;

        for (User line : data) {

            if (line.userEqualsPassword()) {
                equals++;
            }
        }

        return equals / data.size() * 100;

    }

    int dataLeak() {
        return data.size();
    }

    int numberInPass() {

        int number = 0;

        for (User line : data) {

            if (!line.getPassword().matches(".*\\d.*")) {
                number++;
            }

        }

        return number;

    }

    /* [double]` informację jaki % użytkowników nie używało dużych liter w haśle */
    double bigDataInPass() {

        double number = 0;

        for (User line : data) {

            if (!line.getPassword().matches(".*[A-Z].*")) {
                number++;
            }

        }

        return number / data.size() * 100;

    }

    /* [int] informację ile użytkowników miało hasło krótsze niż 5 znaków (sprawdzana długość hasła powinna być parameterem metody) */

    int passwordShorterThan(int value) {

        int number = 0;

        for (User line : data) {

            if (line.getPassword().length() < value) {
                number++;
            }

        }

        return number;

    }

    int passwordShorterThan() {

        return this.passwordShorterThan(5);

    }

    /* [Map<Integer, Integer>] informację jaki był rozkład długości haseł (ile razy wystapiła długość 1, ile 2, ile 3...) */
    Map<Integer, Integer> passwordsLength() {

        Map<Integer, Integer> dane = new TreeMap<>();

        for (User line : data) {

            int passLength = line.getPassword().length();

            if (dane.containsKey(passLength)) {
                dane.put(passLength, dane.get(passLength) + 1);
            } else {
                dane.put(passLength, 1);
            }

        }

        return dane;

    }

    /* [List<String>] listę 10 najpopularniejszych haseł */ // moje rozwiazanie :)
    List<String> popularPasswords() {

        List<String> popular = new ArrayList<>(10);
        Map<String, Integer> dane = new HashMap<>();
        Map<String, Integer> tmpDane = new HashMap<>();

        for (User line : data) {

            String pass = line.getPassword();
            if (dane.containsKey(pass)) {
                dane.put(pass, dane.get(pass) + 1);
            } else {
                dane.put(pass, 1);
            }
        }

        int max = 0;
        for (String s : dane.keySet()) {
            if (dane.get(s) > 1) {
                tmpDane.put(s, dane.get(s));
            }
            if (dane.get(s) > max) {
                max = dane.get(s);
            }
        }

        for (int j = max; j > 0; j--) {
            for (String s : tmpDane.keySet()) {
                if (tmpDane.get(s).equals(j)) {
                    popular.add(s + "_" + j);
                }
                if (popular.size() == 10) {
                    break;
                }
            }
        }

        return popular;

    }

    Map<String, Integer> popularPasswords2() {

        Map<String, Integer> popular = new TreeMap<>();

        for (User line : data) {

            popular.put(
                    line.getPassword(),
                    popular.getOrDefault(line.getPassword(), 0) + 1
            );

        }

        List<Map.Entry<String, Integer>> entries = new ArrayList<>(popular.entrySet());
        entries.sort(Map.Entry.comparingByValue());

        List<Map.Entry<String, Integer>> sublist = entries.subList(entries.size() - 10, entries.size());

        Map<String, Integer> out = new HashMap<>();
        for (Map.Entry<String, Integer> entry : sublist) {
            out.put(
                    entry.getKey(),
                    entry.getValue()
            );
        }

        return out;

    }

    /* [Map<String, Integer>]  ile razy wystąpiła każda z domen pocztowych (oczywiście jeśli loginem był mail) (edited) */
    Map<String, Integer> mailBoxCount() {

        Map<String, Integer> dane = new TreeMap<>();

        for (User line : data) {

            String mail = line.getUsername();

            if (mail.contains("@")) {
                String d = mail.split("@")[1].toLowerCase();
                if (dane.containsKey(d)) {
                    dane.put(d, dane.get(d) + 1);
                } else {
                    dane.put(d, 1);
                }
            }

        }

        return dane;

    }

}
