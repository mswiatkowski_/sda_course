package day_12.Zadanie158;

/*
*ZADANIE #158*
Przygotuj plik .csv (gdzie wartości oddzielone są przecinkami - `,`),
* który będzie zawierał reprezentację obiektów klasy `Osoba`. Metoda ma odczytać plik i zwrócić listę obiektów klasy `Osoba`.
```Jan,Nowak,12,false
Anna,Kowalska,25,true
Kuba,Janowski,66,false
Karolina,Nowa,88,true```
 */

public class Osoba {

    private String imie;
    private String nazwisko;
    private int wiek;
    private boolean aktywny;

    public Osoba(String imie, String nazwisko, int wiek, boolean aktywny) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.aktywny = aktywny;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", wiek=" + wiek +
                ", aktywny=" + aktywny +
                '}';
    }

}
