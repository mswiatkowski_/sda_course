package day_12.Zadanie158;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        try {
            System.out.println(listaOsob("files/zadanie158.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    static List<Osoba> listaOsob(String filePath) throws IOException {

        FileReader file = new FileReader(filePath);
        BufferedReader reader = new BufferedReader(file);
        List<Osoba> listaOsob = new ArrayList<>();

        String linia = reader.readLine();

        while (linia != null) {

            listaOsob.add(osoba(linia));
            linia = reader.readLine();

        }

        return listaOsob;

    }

    private static Osoba osoba (String line) {

        String[] tmp = line.split(",");
        return new Osoba(
                tmp[0],
                tmp[1],
                Integer.parseInt(tmp[2]),
                Boolean.parseBoolean(tmp[3])
        );
    }


}
