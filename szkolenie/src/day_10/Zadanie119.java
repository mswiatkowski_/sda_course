package day_10;

/*
Maciej Czapla - TRENER [2:00 PM]
*ZADANIE #119*
Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca różnicę pomiędzy największa a najmniejszą.
 */

import java.util.Arrays;
import java.util.List;

public class Zadanie119 {

    public static void main(String[] args) {

        List<Integer> lista = Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println(roznica(lista));

    }

    public static int roznica(List<Integer> lista) {

        int max = lista.get(0);
        int min = lista.get(0);

        for (Integer num : lista) {
            if (num > max) {
                max = num;
            }

            if (num < min) {
                min = num;
            }
        }

        return max - min;
    }
}
