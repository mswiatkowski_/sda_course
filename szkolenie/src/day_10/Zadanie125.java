package day_10;

/*
*ZADANIE #125*
Utwórz metodę, która przyjmuje listę liczb,
* a następnie zwraca listę, która będzie zawierała dwie listy.
* Na pozycji `0` mają być elementy (wartości) parzyste, a na pozycji `1` elementy nieparzyste.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie125 {
    public static void main(String[] args) {

        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 2);
        System.out.println(test(lista));

    }

    public static List<List<Integer>> test(List<Integer> lista) {

        List<Integer> parzyste = new ArrayList<>();
        List<Integer> nieparzyste = new ArrayList<>();

        for (Integer value : lista) {
            if (value % 2 == 0) {
                parzyste.add(value);
            } else {
                nieparzyste.add(value);
            }
        }

        return Arrays.asList(parzyste, nieparzyste);

    }
}
