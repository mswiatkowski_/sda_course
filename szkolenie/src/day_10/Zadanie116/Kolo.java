package day_10.Zadanie116;

public class Kolo extends Ksztalt implements Obliczenia {

    protected int promien;

    Kolo(int promien) {
        this.promien = promien;
    }

    @Override
    protected void wyswietlInformacjeOSobie() {
        System.out.println("Promien kola to : " + promien);
    }

    @Override
    public double policzPole() {
        return Math.PI * Math.pow(promien, 2);
    }

    @Override
    public double policzObwod() {
        return 2 * Math.PI * promien;
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return policzPole() * wysokosc;
    }

}
