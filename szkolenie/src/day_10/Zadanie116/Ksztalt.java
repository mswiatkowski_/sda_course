package day_10.Zadanie116;

/*
*ZADANIE #116*
Utwórz klasę `Ksztalt` oraz klasy dziedziczące po niej:
- klasę `Prostokat` (z polami `dlugosc`, `szerokosc`),
- klasę `Koło` (z polem `promien`).
- klasę `Kwadrat` (z polem `Prostokat`).

Zablokuj możliwość tworzenia obiektów klasy `Ksztalt` oraz wymuś, by klasy dziedziczące musiały zaimplementować metodę `wyswietlInformacjeOSobie()`.
*
* Utwórz interfejs `Obliczenia` w którym będą metody `policzPole()`, `policzObwod()` oraz `policzObjetosc(int wysokosc)`. Interfejs ten zaimplementuj we wcześniej utworzonych klasach. (edited)
 */

public abstract class Ksztalt {

    protected abstract void wyswietlInformacjeOSobie();

}
