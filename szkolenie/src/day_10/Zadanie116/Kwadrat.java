package day_10.Zadanie116;

public class Kwadrat extends Prostokat{

    Kwadrat(int bok) {
        super(bok, bok);
    }

    @Override
    protected void wyswietlInformacjeOSobie() {
        System.out.println("Boki kwadratu to : " + dlugosc + " i " + szerokosc);
    }

}
