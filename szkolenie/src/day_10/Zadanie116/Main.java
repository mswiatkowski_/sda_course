package day_10.Zadanie116;

public class Main {

    public static void main(String[] args) {

        Kolo kolo = new Kolo(4);
        Kwadrat kwadrat = new Kwadrat(4);
        Prostokat prostokat = new Prostokat(4,5);

        kolo.wyswietlInformacjeOSobie();
        prostokat.wyswietlInformacjeOSobie();
        kwadrat.wyswietlInformacjeOSobie();

        System.out.println(kwadrat.policzPole());

    }

}
