package day_10.Zadanie116;

public class Prostokat extends Ksztalt implements Obliczenia {

    protected int dlugosc;
    protected int szerokosc;

    Prostokat(int dlugosc, int szerokosc) {
        this.dlugosc = dlugosc;
        this.szerokosc = szerokosc;
    }

    @Override
    protected void wyswietlInformacjeOSobie() {
        System.out.println("Boki prostokątu to : " + dlugosc + " i " + szerokosc);
    }

    @Override
    public double policzPole() {
        return dlugosc * szerokosc;
    }

    @Override
    public double policzObwod() {
        return (2 * dlugosc) + (2 * szerokosc);
    }

    @Override
    public double policzObjetosc(int wysokosc) {
        return 0;
    }

}
