package day_10.Zadanie116;

interface Obliczenia {

    public double policzPole();
    public double policzObwod();
    public double policzObjetosc(int wysokosc);

}
