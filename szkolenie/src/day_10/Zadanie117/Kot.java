package day_10.Zadanie117;

/*
    ZADANIE #117*
    Utwórz klasy `Pies`, `Kot`, `Kon` z polami `imie`, `wiek`, `rasa`
    Utwórz interfejs `Interakcja` w którym będzie metoda `dajGlos()` (edited)
 */

public class Kot implements Interakcja{

    private String imie;
    private int wiek;
    private String rasa;

    Kot(String imie, int wiek, String rasa) {
        this.imie = imie;
        this.wiek = wiek;
        this.rasa = rasa;
    }

    @Override
    public String dajGlos() {
        return "Miał MIał";
    }

}
