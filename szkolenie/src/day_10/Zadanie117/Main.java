package day_10.Zadanie117;

public class Main {

    public static void main(String[] args) {

        Kon kon = new Kon("Rafał", 12, "Arab");
        Pies pies = new Pies("Leszek", 2, "Kundelek");
        Kot kot = new Kot("Bonifacy", 3, "Rudy");

        Interakcja[] zwierzaki = {kon, kot, pies};

        for (int i = 0; i < zwierzaki.length; i++) {
            System.out.println(zwierzaki[i].dajGlos());
            System.out.println(zwierzaki[i].przedstawSie());
        }


    }


}
