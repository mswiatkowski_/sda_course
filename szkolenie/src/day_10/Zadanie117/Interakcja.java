package day_10.Zadanie117;

interface Interakcja {

    public String dajGlos();

    default String przedstawSie() {
        return "Jestem zwierzakiem";
    }

}
