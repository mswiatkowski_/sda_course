package day_10.Zadanie117;

public class Pies implements  Interakcja{

    private String imie;
    private int wiek;
    private String rasa;

    Pies(String imie, int wiek, String rasa) {
        this.imie = imie;
        this.wiek = wiek;
        this.rasa = rasa;
    }

    @Override
    public String dajGlos() {
        return "HAł Hał";
    }

    @Override
    public String przedstawSie() {
        return "Jestem pieskiem a nie zwierzakiem";
    }
}
