package day_10;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
*ZADANIE #121*
Utwórz metodę, która przyjmuje liczbę elementów do wylosowania a następnie zwraca listę (np. `ArrayList`) wypełnioną losowymi wartościami.
 */

public class Zadanie121 {

    public static void main(String[] args) {

        System.out.println(losowe(1));


    }

    public static List<Integer> losowe(int liczbaElementow) {

        List<Integer> lista = new ArrayList<>();

        Random rand = new Random();

        for (int i = 0; i < liczbaElementow; i++) {
            lista.add(rand.nextInt(1001));
        }

        return lista;

    }
}
