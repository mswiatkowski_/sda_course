package day_10;

/*
Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca ich sumę.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie118 {

    public static void main(String[] args) {

        List<Integer> lisat = new ArrayList<>();
        lisat.add(2);
        lisat.add(6);
        lisat.add(3);
        lisat.add(32);
        lisat.add(4);
        lisat.add(0,15);

        System.out.println(sum2(lisat));

    }

    public static int sum(List<Integer> lista) {

        int suma = 0;

        for (int i = 0; i < lista.size(); i++) {
            suma += lista.get(i);
        }

        return suma;
    }

    public static int sum2(List<Integer> lista) {

        int sum = 0;

        for (Integer num: lista) {
            sum += num;
        }

        return sum;
    }

}
