package day_10;

/*
*ZADANIE #120*
Utwórz metodę, która przyjmuje listę (`List`) np. `ArrayList` elementów typu
* `String` a następnie zwraca listę w odwróconej kolejności.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zadanie120 {

    public static void main(String[] args) {

        List<String> lista  = Arrays.asList("1", "2", "3", "4", "5");
        System.out.println(odwrotna(lista));
        System.out.println(odwrotna2(lista));

        Collections.reverse(lista);
        System.out.println(lista);

    }

    public static List<String> odwrotna(List<String> lista) {

        List<String> out = new ArrayList<>();

        for (int i = lista.size() -1; i >= 0; i--) {
            out.add(lista.get(i));
        }

        return out;

    }

    public static List<String> odwrotna2(List<String> lista) {

        List<String> out = new ArrayList<>();
        for (String s : lista) {
            out.add(0, s);
        }

        return out;

    }
}
