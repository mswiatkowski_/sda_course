package day_10;

/*
*ZADANIE #122*
Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania `-1` a następnie zwraca ich listę (np. wykorzystując implementację `ArrayList`)
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie122 {

    public static void main(String[] args) {

        System.out.println(lista());


    }

    public static List<Integer> lista() {

        List<Integer> l = new ArrayList<>();
        Scanner scaner = new Scanner(System.in);

        for (;;) {

            System.out.print("Podaj liczbe : ");
            int dodane = scaner.nextInt();

            if (dodane == -1) {
                break;
            }

            l.add(dodane);
        }

        return l;

    }
}
