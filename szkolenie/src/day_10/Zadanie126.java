package day_10;

/*
*ZADANIE #126*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`) oraz indeks za
* którym należy “przeciąć” tablicę i zwrócić drugą część.
> Dla `3` oraz `<1, 7, 8, 22, 10, -2, 33>`
> powinno zwrócić `<10, -2, 33>`
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie126 {

    public static void main(String[] args) {

        List<Integer> lista = Arrays.asList(1, 7, 8, 22, 10, -2, 33);
        System.out.println(test(lista, 30));

    }

    public static List<Integer> test(List<Integer> lista, int value) {

        List<Integer> out = new ArrayList<>();

        for (int i = value + 1; i < lista.size(); i++) {
            out.add(lista.get(i));
        }

        return out;

    }
}
