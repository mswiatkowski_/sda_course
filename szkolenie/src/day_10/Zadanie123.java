package day_10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*

*ZADANIE #123*
Utwórz metodę, która przyjmuje dwa parametry - listę liczb *całkowityh* (np. `ArrayList`)
oraz poszukiwaną liczbę. Metoda ma zwrócić listę indeksów pod którymi występuje poszukiwana liczba.
>
```10,  110,   2,  10,  1,  5,  8,  -11,  10,  3
XX              XX                     XX```

 */
public class Zadanie123 {
    public static void main(String[] args) {

        List<Integer> lista = Arrays.asList(1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 2);
        System.out.println(ile(lista, 2));
        System.out.println(ile2(lista, 2));

    }

    public static List<Integer> ile(List<Integer> lista, int liczba) {

        List<Integer> counter = new ArrayList<>();

        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) == liczba) {
                counter.add(i);
            }
        }

        return counter;

    }

    public static List<Integer> ile2(List<Integer> lista, int liczba) {

        int counter = 0;
        List<Integer> out = new ArrayList<>();

        for (Integer num : lista) {
            if (num == liczba) {
                out.add(counter);
            }
            counter++;
        }

        return out;

    }
}
