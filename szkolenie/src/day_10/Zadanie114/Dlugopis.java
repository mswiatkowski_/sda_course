package day_10.Zadanie114;

public class Dlugopis extends Produkt{

    Dlugopis (String rzecz) {
        super(rzecz);
    }

    @Override
    protected void wyswietlInformacje() {
        System.out.println(String.format("Jestem dlugopisem o nazwie: %s", this.rzecz));
    }

}
