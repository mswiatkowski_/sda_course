package day_10.Zadanie114;

public class Main {

    public static void main(String[] args) {

        // Produkt produkt = new Produkt("rower"); nie mozna utworzyc obiektu (klasa abstrakcyjna)
        Plecak plecak = new Plecak("plecak Nike");
        Dlugopis dlugopis = new Dlugopis("dlugopis Parker");
        Kasiazka ksiazka = new Kasiazka("Ogniem i mieczem");

        Produkt[] obiekty = {plecak, dlugopis, ksiazka};

        for (int i = 0; i < obiekty.length; i++) {
            obiekty[i].wyswietlInformacje();
        }

    }

}
