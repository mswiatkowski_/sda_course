package day_10.Zadanie114;

public class Plecak extends Produkt {

    Plecak (String rzecz) {
        super(rzecz);
    }

    @Override
    protected void wyswietlInformacje() {
        System.out.println(String.format("Jestem ksiazka o nazwie: %s", this.rzecz));
    }

}
