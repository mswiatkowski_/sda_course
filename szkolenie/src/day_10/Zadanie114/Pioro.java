package day_10.Zadanie114;

public class Pioro extends Produkt {

    Pioro(String rzecz) {
        super(rzecz);
    }

    @Override
    protected void wyswietlInformacje() {
        System.out.println(String.format("Jestem piorem o nazwie: %s", this.rzecz));
    }

}
