package day_1;

public class zadanie6 {
    public static void main(String[] args) {
        System.out.println(sum(2, 11));
        System.out.println(sum(-10, 3));

        int result = sum(3, 5);
        System.out.println(result);
    }

    static int sum(int num1, int num2) {
        return num1 + num2;
    }
}
