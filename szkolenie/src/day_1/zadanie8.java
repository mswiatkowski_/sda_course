package day_1;

public class zadanie8 {
    public static void main(String[] args) {
        System.out.println(pow3(2));
        System.out.println(pow3(349));
        System.out.printf("%.0f", pow3(456));
    }

    static double pow3(int num1){
//        return num1 * num1 * num1;
        return Math.pow(num1, 3);
    }
}
