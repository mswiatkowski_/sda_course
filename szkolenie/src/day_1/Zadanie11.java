package day_1;

public class Zadanie11 {
    public static void main(String[] args) {

        System.out.println(test1("Michał", 22, true));

    }

    static String test1(String data1, int data2, boolean isMan) {
        return String.format("Cześć, jestem %s, mam %s lat i jestem %s.",
                data1,
                data2,
                isMan? "mężczyzną":"kobietą"
        );
    }

}
