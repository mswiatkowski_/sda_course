package day_1;

public class Zadanie13 {
    public static void main(String[] args) {

        System.out.println(isEven(5));
        System.out.println(isEven(3));
        System.out.println(isEven(4));
        System.out.println(isEven(55));
        System.out.println(isEven(76));
        System.out.println(isEven(23));

    }

    static boolean isEven(int num1) {

        return (num1 % 2) == 0;

    }

}
