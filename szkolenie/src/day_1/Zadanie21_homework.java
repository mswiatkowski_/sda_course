package day_1;

public class Zadanie21_homework {
    public static void main(String[] args) {

        System.out.println(check(3, 5, 7));
        System.out.println(check(3, 3, 3));
        System.out.println(check(9, 5, 2));
        System.out.println(check(-7, -7, -7));

    }

    static boolean check(int num1, int num2, int num3) {

        return (num1 == num2 && num1 == num3);

    }

}
