package day_1;

public class zadanie7 {
    public static void main(String[] args) {
        calc(1,3);
    }

    static void calc(int num1, int num2){
        System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
        System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
        System.out.println(num1 + " * " + num2 + " = " + (num1 * num2));
        System.out.println(num1 + " / " + num2 + " = " + ((float) num1 / num2));
        System.out.println("\n");
        System.out.printf("%s + %s = %s\n", num1, num2, num1 + num2);
        System.out.printf("%s - %s = %s\n", num1, num2, num1 - num2);
        System.out.printf("%s * %s = %s\n", num1, num2, num1 * num2);
        System.out.printf("%s / %s = %s\n", num1, num2, (float) num1 / num2);

    }
}
