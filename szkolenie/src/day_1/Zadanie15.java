package day_1;

public class Zadanie15 {

    /**
     *
     * Utwórz metodę, która przyjmuje 4 parametry. Pierwszy i drugi to to godziny i minuty pierwszej z godzin.
     * Natomiast trzeci i czwarty parametr to godziny i minuty drugiej z godzin. Metoda ma zwrócić liczbę sekund pomiędzy godzinami.
     *
     * Czyli dla `15:23` i `22:10` metoda przyjmuje `(15, 23, 22, 10)`
     * Czyli dla `9:33` i `14:59` metoda przyjmuje `(9, 33, 14, 59)`
     *
     */

    public static void main(String[] args) {

        System.out.println(minutes(15, 23,22, 10));
        System.out.println(minutes(9, 33,14, 59));

    }

    static String minutes(int h1, int m1, int h2, int m2) {

        int out = ((h2 -h1)*60) + (m2 - m1);
        return String.format("Różnica: %sh %sm", (out / 60), (out%60));

    }

}
