package day_1;

public class Zadanie12 {
    public static void main(String[] args) {

        System.out.println(maxnum(1, 2, 3));
        System.out.println(maxnum2(3, 2, 9));
        System.out.println(maxnum3(9, 33, 16));
        System.out.println(maxnum4(19, 17, 16));

    }

    static int maxnum(int num1, int num2, int num3) {

        int test = Math.max(num1, num2);
        return Math.max(test, num3);

    }

    static int maxnum2(int num1, int num2, int num3) {

        return Math.max(Math.max(num1, num2), num3);

    }

    static int maxnum3(int num1, int num2, int num3) {

        if (num1 > num2 && num1 > num3) {
            return num1;
        } else if (num2 > num1 && num2 > num3) {
            return num2;
        } else {
            return num3;
        }

    }

    static int maxnum4(int a, int b, int c) {

        return (a > b) ? (a > c ? a : c) : (b > c ? b : c);

    }

}