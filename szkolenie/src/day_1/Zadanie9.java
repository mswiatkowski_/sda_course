package day_1;

public class Zadanie9 {
    public static void main(String[] args) {
        System.out.println(test(6));
        System.out.println(test(22));

        System.out.println(test2(22));
        System.out.println(test3(19));
        System.out.println(test4(20));
    }

    static boolean test(int num1) {
        if (num1 >= 20) {
            return true;
        } else {
            return false;
        }
    }

    static String test2(int num1) {
        if (num1 >= 20) {
            return "TAK, liczba " + num1 + " jest większa od 20";
        } else {
            return "Liczba " + num1 + " jest mniejsza od 20";
        }
    }

    static String test3(int num1) {
        if (num1 >= 20) {
            return String.format("TAK, liczba %s jest większa lub równa 20", num1);
        } else {
            return String.format("Liczba %s jest mniejsza od 20", num1);
        }
    }

    static String test4(int num1) {
        if (num1 > 20) {
            return String.format("TAK, liczba %s jest większa od 20", num1);
        } else if(num1 == 20) {
            return String.format("Wprowadzona liczba jest równa 20", num1);
        } else {
            return String.format("Liczba %s jest mniejsza od 20", num1);
        }
    }

    static String test5(int num1) {

        String out;

        if (num1 > 20) {
            out = "większa od";
        } else if (num1 == 20) {
            out = "równa";
        } else {
            out = "mniejsza od";
        }

        return String.format("Liczba %s jest %s 20", num1, out);

    }

}
