package day_1;

public class Zadanie14 {
    public static void main(String[] args) {

        System.out.println(isEven(23, 23));
        System.out.println(isEven(13, 476));
        System.out.println(isEven(36, 334));

        System.out.println(isEven1(23, 23));
        System.out.println(isEven1(13, 476));
        System.out.println(isEven1(36, 334));

    }

    static boolean isEven(int num1, int num2) {

        return ((num1 % 2 == 0) && (num2 % 2 == 0));

    }

    static boolean isEven1(int num1, int num2) {

        return ((num1 % 2 == 0) || (num2 % 2 == 0));

    }

}
