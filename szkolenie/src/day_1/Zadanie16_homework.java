package day_1;

public class Zadanie16_homework {
    public static void main(String[] args) {

        System.out.println(brutto(100, 23));
        System.out.println(brutto(199, 8));

    }

    static double brutto(double netto, double vat) {

        return (netto * (vat / 100)) + netto;

    }

}
