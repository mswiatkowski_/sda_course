package day_1;

public class Zadanie10 {
    public static void main(String[] args) {

        System.out.println("Liczba " + test1(3, 4) + " jest mniejsza");
        System.out.println("Liczba " + test2(-1, 22) + " jest mniejsza");
        System.out.println("Liczba " + test3(-4, -433) + " jest mniejsza");

    }

    static int test1(int num1, int num2) {

        if (num1 < num2) {
            return num1;
        } else {
            return num2;
        }

    }

    static int test2(int num1, int num2) {

        return Math.min(num1, num2);

    }

    static int test3(int num1, int num2) {

        return (num1 < num2)? num1:num2;

    }

}
