package day_1;

public class Zadanie17_homework {
    public static void main(String[] args) {

        System.out.println(dojob(1, 2, 3, 8));
        System.out.println(dojob(10, 11, 12, 1));
        System.out.println(dojob(8, 8, 8, 3));

    }

    // a^(x) + b^(x+1) + c^(x+2)

    static double dojob(int num1, int num2, int num3, int num4) {

        return Math.pow(num1, num4) + Math.pow(num2, num4 + 1) + Math.pow(num3, num4 + 2);

    }

}
