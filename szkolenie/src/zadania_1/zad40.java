package zadania_1;

public class zad40 {
    public static void main(String[] args) {

        System.out.println(test(1234));

    }

    /**
     *
     * Welcome. In this kata, you are asked to square every digit of a number.
     *
     * For example, if we run 9119 through the function, 811181 will come out, because 92 is 81 and 12 is 1.
     *
     * Note: The function accepts an integer and returns an integer
     *
     */

    static int test(int num) {

        int divided = num;
        String out = "";

        while(divided != 0) {

            int div2 = divided % 10;

            divided /= 10;

            out = (div2 * div2) + out;

        }

        return  Integer.parseInt(out);

    }

}
