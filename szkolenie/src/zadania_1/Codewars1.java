package zadania_1;

/*
I would like to be able to pass an array with two elements to my swapValues function to swap the values. However it appears that the values aren't changing.

Can you figure out what's wrong here?

https://www.codewars.com/kata/swap-values/train/java

 */

public class Codewars1 {

    public Object[] arguments;

    public Codewars1(final Object[] args) {
        arguments = args;
    }

    public void swapValues() {
        Object[] args = new Object[]{arguments[0], arguments[1]};
        Object temp = args[0];
        args[0] = args[1];
        args[1] = temp;
        arguments = args;
    }

    public void swapValues2() {
        Object temp = arguments[0];
        arguments[0] = arguments[1];
        arguments[1] = temp;
    }

    public void swapValues3() {
        arguments = new Object[]{arguments[1], arguments[0]};
    }

}
