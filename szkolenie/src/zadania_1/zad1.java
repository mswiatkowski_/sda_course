package zadania_1;

public class zad1 {
    public static void main(String[] args) {
        System.out.println(repeatStr(3, "michal"));
        System.out.println(summation(8));
    }

    public static String repeatStr(final int repeat, final String string) {
        String out = "";
        for (int i = 1; i <= repeat ; i++) {
            out += string;
        }
        return out;
    }

    public static int summation(int n) {

        int s = 0;

        for (int i = 1; i <= n; i++) {

            s += i;

        }

        return s;
    }
}
