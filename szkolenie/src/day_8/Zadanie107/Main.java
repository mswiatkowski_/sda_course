package day_8.Zadanie107;

public class Main {
    public static void main(String[] args) {

        Tablica tab = new Tablica(6);
        tab.showArray();
        tab.putLastElement(54);

        System.out.println();

        tab.spreadArray(2);
        tab.putIndex(23,4);
        tab.putIndex(2,2);
        tab.putIndex(11,3);
        tab.putFirstElement(34);
        tab.showArray();

        //tab.removeIndex(5);

        System.out.println("MAX: " +  tab.maxValue());

    }
}
