package day_8.Zadanie107;

/*
*ZADANIE #107*
Utwórz klasę `Tablica` służącą do obsługi tablicy jednowymiarowej z możliwością dynamicznej zmiany rozmiaru.
>*konstruktor:*
>* jednoparametrowy (przyjmujący początkowy rozmiar tablicy)
>
>*pole:*
>* będące tablicą (ale z modyfikatorem `private`!!!)
>
>*metody umożliwiające:*
>* Wstawienie elementu na koniec
>* Zwrócenie rozmiaru tablicy
>* Wyświetlenie tablicy
>* Rozszerzenie tablicy o wybraną liczbę pozycji na końcu (domyślnie o `1`)
>* Wstawienie elementu na początek
>* Wstawienie elementu pod wybranym indeksem
>* Usunięcie elementu pod wybranym indeksem
>* Zwrócenie największego elementu w tablicy
 */

public class Tablica {

    private Integer[] tab;

    Tablica(int size) {
        tab = new Integer[size];
    }

    void putLastElement(int element) {
        tab[tab.length - 1] = element;
    }

    int returnArraySize() {
        return tab.length;
    }

    void showArray() {

        for (int i = 0; i < tab.length; i++) {
            System.out.println(i + ". " + tab[i]);
        }

    }

    void spreadArray(int newsize) {

        if (newsize < 1) {
            return;
        }

        Integer [] newArray = new Integer[tab.length + newsize];
        for (int i = 0; i < tab.length; i++) {
            newArray[i] = tab[i];
        }

        tab = newArray;

    }

    void spreadArray() {
        spreadArray(1);
    }

    void putFirstElement(int element) {
        tab[0] = element;
    }

    void putIndex(int element, int index) {
        tab[index - 1] = element;
    }

    void removeIndex(int index) {
        tab[index] = null;
    }

    Integer maxValue() {
        Integer max = null;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] != null) {
                if (max == null) {
                    max = tab[i];
                } else if (tab[i] > max){
                    max = tab[i];
                }
            }
        }
        return max;
    }

}
