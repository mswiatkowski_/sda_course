package day_8.Zadanie109;

/*
*ZADANIE #109*
Utwórz typ wyliczeniowy `TypWaluty` zawieająca opcje `USD`, `EUR`, `PLN` wraz z opisem
* (`Dolar`, `Euro`, `Polski Złoty`). Utwórz klasę `Waluta`, gdzie jednym z pól będzie
* ten typ (oraz pola `kursSprzedazy`, `kursKupna`).
 */

public class Waluta {

    private Currency typ;
    private double kursSprzedazy;
    private double kursKupna;

    Waluta(Currency typ, double kursSprzedazy, double kursKupna) {
        this.typ = typ;
        this.kursKupna = kursKupna;
        this.kursSprzedazy = kursSprzedazy;
    }

    public String toString() {
        return this.kursSprzedazy + " " + this.kursKupna + " " + typ.getOpis();
    }

}