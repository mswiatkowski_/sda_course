package day_8.Zadanie109;

public enum Currency {

    EURO("Euro"),
    PLN("Złoty"),
    USD("Dolar");

    private String opis;

    Currency(String opis) {
        this.opis = opis;
    }

    public String getOpis() {
        return opis;
    }
}
