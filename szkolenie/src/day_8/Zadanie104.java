package day_8;

/*
*ZADANIE #104*
Utwórz metodę, która wczyta od użytkownika *dwie* liczby a następnie zwróci ich sumę. Wykorzytaj klasę `Scanner`
 */

import java.util.Scanner;

public class Zadanie104 {

    public static void main(String[] args) {

        sum();

    }

    static void sum() {

        Scanner scanner = new Scanner(System.in);

        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        System.out.println("Suma to: " + (num1 + num2));

    }

}