package day_8.Zadanie108;

/*
*ZADANIE #108*
Utwórz typ wyliczeniowy `Kolor` (zawierająca kilka opcji). Utwórz klasę `Samochod`, gdzie jednym z pól będzie ten typ (pozostałe to np. `marka`, `rokProdukcji` czy `liczbaDrzwi`).
 */

public class Samochod {

    private Color color;
    private String marka;

    Samochod(Color color, String marka) {
        this.color = color;
        this.marka = marka;
    }

    public String toString() {
        return this.marka + " " + color;
    }

}

enum Color {
    WHITE, BLACK, BROWN, ORANGE, RED, BLUE, PURPLE, GREEN
}
