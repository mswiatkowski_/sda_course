package day_8.Zadanie110;

/*
*ZADANIE #110*
Utwórz klasę `Pobieranie` służącą do pobierania danych z konsoli.
>*metody:*
>* `pobierzInt()`
>* `pobierzString()`
>
> Dodaj możliwość przekazania wartości logicznej, od której zależy wyświetlenie komunikatu na konsoli (np. `Podaj wartość typu String:` ).
>
>Użytkownik może zadeklarować obiekt nowo utworzonej klasy u siebie w projekcie:
>
Pobieranie p = new Pobieranie();
p.pobierzInt(true)
 */

import java.util.Scanner;

public class Pobieranie {

    private Scanner scanner = new Scanner(System.in);

    public void pobierzInt(boolean option) {
        int value;

        for(;;) {
            try {
                System.out.print((option? "Podaj wartość typu int: " : ""));
                value = this.scanner.nextInt();
                break;
            } catch (java.util.InputMismatchException e) {
                System.out.println("Wprowadziłeś wartość nie jest cyfrą!");
            }
        }

        System.out.println("Wprowadziłeś wartość int = " + value);
        this.scanner.nextLine();

    }

    public void pobierzInt() {

        this.pobierzInt(false);

    }

    public void pobierzString(boolean option) {

        String value;

        System.out.print((option? "Podaj wartość typu String: " : ""));
        value = this.scanner.nextLine();

        System.out.println("Wprowadziłeś String = " + value);

    }

    public void pobierzString() {

        this.pobierzString(false);

    }

}
