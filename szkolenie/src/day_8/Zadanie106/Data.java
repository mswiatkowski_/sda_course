package day_8.Zadanie106;

/*

*ZADANIE #106*
Utwórz klasę `Date` (`Data`) posiadającą
>*Pola reprezentujące:*
>* dzień
>* miesięc
>* rok
>
>*Następujący kostruktor:*
>* 3-parametrowy (przyjmujący wszystkie parametry)
>
>*Metody umożliwiające:*
>* ustawienie dnia (wraz ze sprawdzaniem poprawności) - zwracająca typ `boolean`
>* ustawienie miesiąca (wraz ze sprawdzaniem poprawności) - zwracająca typ `boolean`
>* ustawienie roku (wraz ze sprawdzaniem poprawności) - zwracająca typ `boolean`
>* wyświetlenie daty w formacie `dzien-miesiac-rok` (opcjonalnie z możliwością ustawienia separatora)
>* wyświetlenie daty w formacie `rok-miesiac-dzien` (opcjonalnie z możliwością ustawienia separatora)
*
 */

public class Data {

    private int year;
    private int month;
    private int day;
    private final String SEPARATOR = "-";

    Data(int year, int month, int day) {
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    boolean setDay(int day) {
        if (day > 0 && day <= 31) {
            this.day = day;
            return true;
        }

        return false;
    }

    boolean setMonth(int month) {
        if (month > 0 && month <= 12) {
            this.month = month;
            return true;
        }

        return false;

    }

    boolean setYear(int year) {

        if (String.valueOf(year).matches("[0-9]{4}")) {
            this.year = year;
            return true;
        }

        return false;

    }

    void showDate(String separator) {

        System.out.println(getDate(separator));

    }

    private String getDate(String separator) {

        return this.day + separator + this.month + separator + this.year;

    }

    void showDate() {

        this.showDate(SEPARATOR);

    }

    public String toString() {

        return getDate(SEPARATOR);

    }

}
