package day_8;

/*
*ZADANIE #105*
Utwórz metodę, która będzie wczytywała liczby od użytkownika do momentu podania liczby ujemnej a następnie wyświetli sumę oraz średnią wszystkich podanych liczb (bez uwzględniania tej ujemnej).
 */

import java.util.Scanner;

public class Zadanie105 {
    public static void main(String[] args) {

        sum();

    }

    static void sum() {

        int sum = 0;
        Scanner scanner = new Scanner(System.in);
        int counter = 0;

        for (; ; ) {

            int num = scanner.nextInt();
            if (num < 0) {
                System.out.println(sum + " " + ((double) sum / counter));
                break;
            }

            counter++;
            sum += num;

        }

    }

    public static void sumAndAvg() {
        Scanner sc = new Scanner(System.in);
        int number;
        int sum = 0;
        int counter = 0;

        while (true) {
            number = sc.nextInt();
            if (number < 0) {
                break;
            }
            sum += number;
            counter++;
        }
        double avg = counter == 0 ? 0.0 : (double) sum / counter;
        System.out.println("Suma: " + sum + " Srednia: " + avg);
    }

    public static void sumAndAvgDoWhile() {
        Scanner sc = new Scanner(System.in);
        int number = 0;
        int sum = 0;
        int counter = -1;
        do {
            sum += number;
            counter++;
            number = sc.nextInt();
        }
        while (number >= 0);
        double avg = counter == 0 ? 0.0 : (double) sum / counter;
        System.out.println("Suma: " + sum + " Srednia: " + avg);
    }

    public static void sumAndAvgWhile() {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        int sum = 0;
        int counter = 0;

        while (number >= 0) {
            sum += number;
            counter++;
            number = sc.nextInt();
        }
        double avg = counter == 0 ? 0.0 : (double) sum / counter;
        System.out.println("Suma: " + sum + " Srednia: " + avg);
    }

}
