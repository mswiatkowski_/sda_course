package day_19.fasada;

public class Fasada {

    private Okno okno1 = new Okno();
    private Okno okno2 = new Okno();
    private Swiatlo swiatlo1 = new Swiatlo();
    private Swiatlo swiatlo2 = new Swiatlo();

    void wejsc() {
        okno1.otwarcie();
        okno2.otwarcie();
        swiatlo1.wlaczone();
        swiatlo2.wlaczone();
    }

    void wyjsc() {
        okno1.zamkniecie();
        okno1.zamkniecie();
        swiatlo1.wylaczone();
        swiatlo2.wylaczone();
    }

}
