package day_19.obserwator;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

class Weather {

    private List<Person> subscribers = new LinkedList<>();
    private int systemTemperature = 26;

    void addToList(Person person) {
        subscribers.add(person);
    }

    void addToList(Person... person) {
        subscribers.addAll(Arrays.asList(person));
    }

    void updateTemperature(int temp) {
        systemTemperature = temp;
        notifySubscribers();
    }

    private void notifySubscribers() {

        for (Person subscriber : subscribers) {
            subscriber.notifyUser(systemTemperature);
        }

    }

}
