package day_19.obserwator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Person kuba = new Person("Kuba", 35);
        Person adam = new Person("Adam", 19);
        Person ola = new Person("Ola", 25);

        Weather weather = new Weather();
        weather.addToList(kuba, adam, ola);

        Scanner scanner = new Scanner(System.in);

        while (true) {

            int value = scanner.nextInt();
            weather.updateTemperature(value);

            if (value > 100) {
                break;
            }

        }

    }

}
