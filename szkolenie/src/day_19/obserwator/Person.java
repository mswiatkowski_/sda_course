package day_19.obserwator;

public class Person {

    private String name;
    private int maxTemperature;

    public Person(String name, int maxTemperature) {
        this.name = name;
        this.maxTemperature = maxTemperature;
    }

    public void notifyUser(int systemTemp) {
        if (systemTemp > maxTemperature) {
            System.out.printf("%s mowi ze jest za cieplo\n", name);
        }
    }
}
