package day_19.strategy;

public class OperationAdd implements Strategy {

    @Override
    public int doOperation(int value1, int value2) {
        return value1 + value2;
    }

}
