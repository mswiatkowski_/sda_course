package day_19.strategy;

public class Main {

    public static void main(String[] args) {

        Strategy strategy = new OperationAdd();
        Calculator calculator = new Calculator(strategy);

        System.out.println(calculator.calculate(34, 34));

        Strategy strategy1 = new OperationSubstract();

        calculator.setStrategy(strategy1);
        System.out.println(calculator.calculate(34, 34));

    }

}
