package day_19.strategy;

public interface Strategy {

    int doOperation(int value1, int value2);

}
