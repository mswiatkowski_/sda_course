package day_19.strategy;

public class Calculator {

    private Strategy strategy;

    Calculator(Strategy strategy) {
        this.strategy = strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    int calculate(int value1, int value2) {
        return strategy.doOperation(value1, value2);
    }

}
