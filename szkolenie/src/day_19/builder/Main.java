package day_19.builder;

public class Main {

    public static void main(String[] args) {

        User user1 = new User.UserBuilder()
                .setAge(22)
                .setName("Michal")
                .build();

//        System.out.println(user1);

        User user2 = new User.UserBuilder()
                .setAge(17)
                .setName("Ewa")
                .setMale(false)
                .setPhone("600500400")
                .build();

//        System.out.println(user2);

//        dodane
        User.printLine(user1, user2);

    }

}
