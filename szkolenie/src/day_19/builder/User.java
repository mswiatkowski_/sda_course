package day_19.builder;

import java.util.Arrays;

public class User {

    private String imie;
    private int wiek;
    private boolean plec;
    private String telefon;

    private User(UserBuilder ub) {
        this.imie = ub.imie;
        this.wiek = ub.wiek;
        this.plec = ub.plec;
        this.telefon = ub.telefon;
    }

    @Override
    public String toString() {
        return "User{" +
                "imie='" + imie + '\'' +
                ", wiek=" + wiek +
                ", plec=" + plec +
                ", telefon='" + telefon + '\'' +
                '}';
    }

    static void printLine(User... users) {

        Arrays.stream(users).forEach(
                user -> System.out.println(user)
        );

    }

    static class UserBuilder {

        private String imie;
        private int wiek;
        private boolean plec = true;
        private String telefon = "Brak";


        UserBuilder setName(String name) {
            this.imie = name;
            return this;
        }

        UserBuilder setAge(int age) {
            this.wiek = age;
            return this;
        }

        UserBuilder setMale(boolean male) {
            this.plec = male;
            return this;
        }

        UserBuilder setPhone(String phone) {
            this.telefon = phone;
            return this;
        }

        User build() {
            return new User(this);
        }

    }

}
