package day_19.singleton;

public class Main {
    public static void main(String[] args) {

        Singleton sing1 = Singleton.getInstance();
        System.out.println(sing1);

        Singleton.getInstance().setValue(4);

        Singleton sing2 = Singleton.getInstance();
        System.out.println(sing2);

        test();

    }

    private static void test() {

        System.out.println(Singleton.getInstance().getValue());
        System.out.println(Singleton.getInstance());

    }


}
