package day_15.Zadanie177;

import day_15.Zadanie177.pkg1.Apple;
import day_15.Zadanie177.pkg1.AppleBox;
import day_15.Zadanie177.pkg1.Orange;
import day_15.Zadanie177.pkg1.OrangeBox;
import day_15.Zadanie177.pkg2.ObjectFruitBox;
import day_15.Zadanie177.pkg3.GenericBox;
import day_15.Zadanie177.pkg4.BiggerGenericBox;

public class Main {

    public static void main(String[] args) {
        metodat2();
    }

    public static void metoda1() {

        Apple apple = new Apple();
        AppleBox applebox = new AppleBox(apple);

        Orange orange = new Orange();
        OrangeBox orangebox = new OrangeBox(orange);

    }

    public static void metodat2() {

//        ObjectFruitBox fruit1 = new ObjectFruitBox(new Apple());
//        System.out.println(fruit1.getFruit().getClass().getSimpleName());
//
//        fruit1.setFruit(new Orange());
//        System.out.println(fruit1.getFruit().getClass().getSimpleName());
//
//        fruit1.setFruit("TEST");
//        System.out.println(fruit1.getFruit().getClass().getSimpleName());
//
//        fruit1.setFruit(true);
//        System.out.println(fruit1.getFruit().getClass().getSimpleName());
//
//        GenericBox<Orange> box = new GenericBox<>(new Orange());
//        box.setElement(new Orange());
//        System.out.println(box.getElement().getClass().getSimpleName());
//
//        BiggerGenericBox<Apple, Orange> box2 = new BiggerGenericBox<>(new Apple(), new Orange());
//        System.out.println(box2.getFirst().getClass().getSimpleName());
//        System.out.println(box2.getSecond().getClass().getSimpleName());
//
//        BiggerGenericBox<Apple, Orange> box3 = new BiggerGenericBox<>();
//        box3.setFirst(new Apple());
//        box3.setSecond(new Orange());
//        System.out.println(box2.getFirst().getClass().getSimpleName());
//        System.out.println(box2.getSecond().getClass().getSimpleName());
//
//
//        BiggerGenericBox<String, Float> box4 = new BiggerGenericBox<>();
//        box4.setFirst("dw");
//        box4.setSecond(12f);
//
//        System.out.println(box4.getFirst().getClass().getSimpleName());
//        System.out.println(box4.getSecond().getClass().getSimpleName());

    }

}
