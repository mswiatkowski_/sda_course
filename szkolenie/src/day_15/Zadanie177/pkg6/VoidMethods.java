package day_15.Zadanie177.pkg6;

import day_15.Zadanie177.pkg5.Circle;
import day_15.Zadanie177.pkg5.Rectangle;
import day_15.Zadanie177.pkg5.ShapeBox;

public class VoidMethods {

    public static void metoda1(ShapeBox<Circle> box) {

        System.out.println(box);

    }

    public static void metoda2(ShapeBox<?> box) {

        System.out.println(box);

    }

    static void metoda3(ShapeBox<? extends Rectangle> box) {

        System.out.println(box);

    }

    static void metoda4(ShapeBox<? super Rectangle> shape) {

        System.out.println(shape);

    }

}
