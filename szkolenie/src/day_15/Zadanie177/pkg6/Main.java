package day_15.Zadanie177.pkg6;

import day_15.Zadanie177.pkg5.Circle;
import day_15.Zadanie177.pkg5.Rectangle;
import day_15.Zadanie177.pkg5.ShapeBox;
import day_15.Zadanie177.pkg5.Square;

public class Main {

    public static void main(String[] args) {

        metoda2();

    }

    static void metoda() {

        System.out.println(ReturnMethods.getParameter(4));
        System.out.println(ReturnMethods.getParameter("testy"));

        System.out.println(ReturnMethods.getShape(new Square()).getName());

        System.out.println(ReturnMethods.returnString("TEST", 34));

        System.out.println(ReturnMethods.getClassName(new Rectangle(), new Square()));



    }

    static void metoda2() {

        ShapeBox<Circle> box = new ShapeBox<>(new Circle());
        VoidMethods.metoda1(box);

        ShapeBox<Rectangle> box2 = new ShapeBox<>(new Rectangle());
        VoidMethods.metoda2(box2);

        VoidMethods.metoda4(box2);

    }

}
