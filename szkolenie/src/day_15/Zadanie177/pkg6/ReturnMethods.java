package day_15.Zadanie177.pkg6;

import day_15.Zadanie177.pkg5.Shape;
import day_15.Zadanie177.pkg5.Square;

public class ReturnMethods {

    public static <TYPE> TYPE getParameter(TYPE parameter) {
        return parameter;
    }

    public static <TYPE extends Shape> TYPE getShape(TYPE parameter) {
        return parameter;
    }

    public static <T> String returnString(T first, T second) {

        return "Przekazano: " + first.getClass().getSimpleName() + " " + second.getClass().getSimpleName();

    }

    public static <A extends Shape, B extends Square> String getClassName(A shape, B square) {

        return shape.getClass().getSimpleName() + " " + square.getClass().getSimpleName();

    }

}
