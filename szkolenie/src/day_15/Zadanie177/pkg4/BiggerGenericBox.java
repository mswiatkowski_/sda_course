package day_15.Zadanie177.pkg4;

public class BiggerGenericBox <A, B> {

    private A first;
    private B second;

    public BiggerGenericBox(A first, B second) {
        this.first = first;
        this.second = second;
    }

    public BiggerGenericBox() {
    }

    public A getFirst() {
        return first;
    }

    public B getSecond() {
        return second;
    }

    public void setFirst(A first) {
        this.first = first;
    }

    public void setSecond(B second) {
        this.second = second;
    }

}
