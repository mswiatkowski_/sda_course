package day_15.Zadanie177.pkg2;

public class ObjectFruitBox {

    private Object fruit;

    public ObjectFruitBox(Object fruit) {
        this.fruit = fruit;
    }

    public Object getFruit() {
        return fruit;
    }

    public void setFruit(Object fruit) {
        this.fruit = fruit;
    }

}
