package day_15.Zadanie177.pkg5;

public class Main {

    public static void main(String[] args) {

        metoda1();

    }

    static void metoda1() {

//        ShapeBox<Shape> shape = new ShapeBox<>(new Circle());
//        System.out.println(shape);
//
//        ShapeBox<Rectangle> rect = new ShapeBox<>(new Rectangle());
//        System.out.println(rect);
//
//        ShapeBox<Rectangle> square = new ShapeBox<>(new Square());
//        System.out.println(square);
//
//        ShapeBox<Circle> circle = new ShapeBox<>(new Circle());
//        System.out.println(circle);

        RectangleBox<Square> rect = new RectangleBox<>(new Square());
        System.out.println(rect);

        RectangleBox<Rectangle> rect2 = new RectangleBox<>(new Rectangle());
        System.out.println(rect2);

        RectangleBox<Rectangle> rect3 = new RectangleBox<>(new Square());
        System.out.println(rect3);

    }

}
