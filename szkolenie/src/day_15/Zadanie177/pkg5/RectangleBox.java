package day_15.Zadanie177.pkg5;

public class RectangleBox <A extends Rectangle> {

    private A rect;

    public RectangleBox(A rect) {
        this.rect = rect;
    }

    public A getRect() {
        return rect;
    }

    public void setRect(A rect) {
        this.rect = rect;
    }

    @Override
    public String toString() {
        return rect.getName();
    }

}
