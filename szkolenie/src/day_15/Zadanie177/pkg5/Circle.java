package day_15.Zadanie177.pkg5;

public class Circle implements Shape {

    @Override
    public String getName() {
        return "I am circle";
    }

    @Override
    public void introduce() {
        System.out.println("hello circle");
    }
}
