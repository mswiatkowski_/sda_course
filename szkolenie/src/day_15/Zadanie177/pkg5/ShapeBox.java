package day_15.Zadanie177.pkg5;

public class ShapeBox <A extends Shape> {

    private A shape;

    public ShapeBox(A shape) {
        this.shape = shape;
    }

    public A getShape() {
        return shape;
    }

    public void setShape(A shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return shape.getName();
    }
}
