package day_15.Zadanie177.pkg5;

public class Rectangle implements Shape {

    @Override
    public String getName() {
        return "I am rectangle";
    }

    @Override
    public void introduce() {
        System.out.println("Hey Rectangle");
    }
}
