package day_15.Zadanie177.pkg5;

public interface Shape {

    String getName();

    default void introduce() {
        System.out.println("HEllo! I am shape");
    }

}
