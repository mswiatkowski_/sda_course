package day_15.Zadanie177.pkg3;

public class GenericBox <TYPE> {

    private TYPE element;

    public GenericBox(TYPE element) {
        this.element = element;
    }

    public TYPE getElement() {
        return element;
    }

    public void setElement(TYPE element) {
        this.element = element;
    }

}
