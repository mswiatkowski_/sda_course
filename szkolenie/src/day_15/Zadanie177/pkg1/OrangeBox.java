package day_15.Zadanie177.pkg1;

public class OrangeBox {

    private Orange orange;

    public OrangeBox(Orange orange) {
        this.orange = orange;
    }

    public Orange getOrange() {
        return orange;
    }
}
