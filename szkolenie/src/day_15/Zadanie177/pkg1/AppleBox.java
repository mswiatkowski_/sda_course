package day_15.Zadanie177.pkg1;

public class AppleBox {

    private Apple apple;

    public AppleBox(Apple apple) {
        this.apple = apple;
    }

    public Apple getApple() {
        return apple;
    }
}
