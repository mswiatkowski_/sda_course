package day_15.Zadanie178.pkg4;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Egzekutor {

    public static void main(String[] args) {

        ExecutorService ex = Executors.newFixedThreadPool(3);

        for (int i = 0; i < 10; i++) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    System.out.println("Prace podjal watek: " + Thread.currentThread().getId());

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            ex.submit(t);
        }

        ex.shutdown();

    }

}
