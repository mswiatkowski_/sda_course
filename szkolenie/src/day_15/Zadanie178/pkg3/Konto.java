package day_15.Zadanie178.pkg3;

public class Konto {

//    private volatile int saldo;
    private int saldo;

    public Konto(int saldo) {
        this.saldo = saldo;
    }

    public synchronized void wplata(int value) {

        saldo += value;

    }

    public synchronized void wyplata(int value) {

        saldo -= value;

    }

    public int getSaldo() {
        return saldo;
    }
}
