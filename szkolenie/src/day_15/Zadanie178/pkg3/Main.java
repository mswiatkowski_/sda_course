package day_15.Zadanie178.pkg3;

public class Main {
    public static void main(String[] args) {

        Konto konto = new Konto(10000);

        int kwotaWplaty = 50;
        int kwotaWyplaty = 43;
        int ileRazy = 10000;


        Thread wplacacz = new Thread(){
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wplata(kwotaWplaty);
                }
            }
        };

        wplacacz.start();

        Thread wyplacacz = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wyplata(kwotaWyplaty);
                }
            }
        });

        wyplacacz.start();

        try {

            wyplacacz.join();
            wplacacz.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(konto.getSaldo());

    }

}
