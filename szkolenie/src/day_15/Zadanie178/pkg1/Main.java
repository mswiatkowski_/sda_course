package day_15.Zadanie178.pkg1;

public class Main {

    public static void main(String[] args) {

        System.out.println("Thread ident: " + Thread.currentThread().getId());

        ProstyWatek t0 = new ProstyWatek(4);
        t0.start();

        ProstyWatek t1 = new ProstyWatek(6);
        t1.start();

        ProstyWatek t2 = new ProstyWatek(3);
        t2.start();

        try {
            t0.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Zakończono prace.");

    }

}
