package day_15.Zadanie178.pkg1;

public class ProstyWatek extends Thread {

    private int value;

    public ProstyWatek(int value) {
        this.value = value;
    }

    @Override
    public void run() {
        System.out.println("ID: " + Thread.currentThread().getId());

        for (int i = 0; i < value; i++) {

            System.out.println("Thread ID: " + i + " " + Thread.currentThread().getId());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e);
            }

        }
    }

}
