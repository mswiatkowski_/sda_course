package day_15.Zadanie178.pkg2;

public class Watek implements Runnable {

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {

            System.out.println("Watek: " + i + " | " + Thread.currentThread().getId());

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

}
