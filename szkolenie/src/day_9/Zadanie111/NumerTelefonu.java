package day_9.Zadanie111;

/*

*ZADANIE #111*
Utwórz klasę `NumerTelefonu` z polami `numer` oraz `kierunkowy`.
Numer kierunkowy jest typem wyliczeniowym (np. z opcjami `POLSKA`, `NIEMCY`, `ROSJA`)
posiadającym dwie metody (`jakoTekst()` zwracająca np. `+48` oraz `jakoNumer()` zwracająca np. `48`).

 */

public class NumerTelefonu {
    private int numer;
    private Kierunkowy kierunkowy;

    NumerTelefonu(int numer, Kierunkowy kierunkowy) {
        this.kierunkowy = kierunkowy;
        this.numer = numer;
    }

    public Kierunkowy getKierunkowy() {
        return kierunkowy;
    }

    public int getNumer() {
        return numer;
    }
}

enum Kierunkowy {

    POLSKA("48"),
    NIEMCY("49"),
    ROSJA("7");

    private String opis;

    Kierunkowy(String opis) {
        this.opis = opis;
    }

    public String jakoTekst() {
        return "+" + this.opis;
    }

    public int jakoNumer() {
        return Integer.parseInt(this.opis);
    }

    public String getOpis() {
        return opis;
    }

}