package day_9.Zadanie111;

public class Main {

    public static void main(String[] args) {
        NumerTelefonu nr = new NumerTelefonu(54554523, Kierunkowy.POLSKA);
        System.out.println(nr.getKierunkowy().jakoNumer());
        System.out.println(nr.getKierunkowy().jakoTekst());
        System.out.println(nr.getKierunkowy());
        System.out.println(nr);
    }

}
