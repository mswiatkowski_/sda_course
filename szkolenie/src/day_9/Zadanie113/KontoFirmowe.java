package day_9.Zadanie113;

import day_9.Zadanie112.Osoba;

public class KontoFirmowe extends Konto{

    private int oplataZaTransakcje;

    KontoFirmowe(Osoba wlasciciel, int oplataZaTransakcje) {

        super(wlasciciel);
        this.oplataZaTransakcje = oplataZaTransakcje;

    }

    KontoFirmowe(Osoba wlasciciel) {
        this(wlasciciel, 2);
    }

    @Override
    public boolean wyplata(double kwota) {
        return super.wyplata(kwota + oplataZaTransakcje);
    }



}
