package day_9.Zadanie113;

import day_9.Zadanie112.Osoba;

public class Main {

    public static void main(String[] args) {

        test_konto();
        System.out.println();
        System.out.println();
        kontoFirmowe();
        System.out.println();
        System.out.println();
        kontoOszczednosciowe();
        System.out.println();
        System.out.println();
        kontoDebetowe();

    }

    public static void test_konto() {


        Osoba osoba = new Osoba("Janusz", 34);
        Konto kontoJanusza = new Konto(osoba);

        Osoba osoba1 = new Osoba("Marian", 14);
        Konto kontomariana = new Konto(osoba1);


        System.out.println(kontoJanusza);
        System.out.println(kontomariana);

        kontoJanusza.wplata(3000);
        System.out.println(kontoJanusza);

        System.out.println(Konto.przelew(kontoJanusza, kontomariana, 300));

        System.out.println(kontoJanusza);

        System.out.println(kontomariana);
        System.out.println(kontomariana.wyplata(100));
        System.out.println(kontomariana);



    }

    public static void kontoFirmowe() {

        Osoba osoba = new Osoba("Januszek", 3);
        KontoFirmowe kontoJanusza = new KontoFirmowe(osoba);

        Osoba osoba1 = new Osoba("Marianek", 45);
        KontoFirmowe kontomariana = new KontoFirmowe(osoba1);

        Osoba osoba2 = new Osoba("Roman", 23);

        System.out.println(kontoJanusza);
        System.out.println(kontomariana);

        kontoJanusza.wplata(3000);
        System.out.println(kontoJanusza);
        kontoJanusza.wyplata(250);
        System.out.println(kontoJanusza);

        KontoFirmowe newKonto = new KontoFirmowe(osoba2, 10);
        newKonto.wplata(3000);
        System.out.println(newKonto);
        newKonto.wyplata(200);
        System.out.println(newKonto);

    }

    public static void kontoOszczednosciowe() {

        Osoba osoba = new Osoba("Janina", 54);
        Osoba osoba1 = new Osoba("Marianna", 44);

        KontoOszczednosciowe kontoJaniny = new KontoOszczednosciowe(osoba, 0.05);
        KontoOszczednosciowe kontoMarianny = new KontoOszczednosciowe(osoba1, 0.05);

        kontoJaniny.wplata(100);
        System.out.println(kontoJaniny);
        System.out.println(kontoMarianny);

        Konto.przelew(kontoJaniny, kontoMarianny, 10);

        System.out.println(kontoJaniny);
        System.out.println(kontoMarianny);


    }


    public static void kontoDebetowe() {

        Osoba osoba = new Osoba("Barbara", 31);

        KontoDebetowe konto1 = new KontoDebetowe(osoba, 100);
        System.out.println(konto1);
        konto1.wyplata(90);
        System.out.println(konto1);

    }

}
