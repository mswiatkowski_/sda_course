package day_9.Zadanie113;

import day_9.Zadanie112.Osoba;

/*

*ZADANIE #113*
Utwórz klasę `Konto`, która zawiera pola `stan`, `numer`, `wlasciciel` oraz metody umożliwiające wpłatę, wypłatę, przelewanie środków, sprawdzenie stanu.

Utwórz klasę `KontoOszczednosciowe` (która dziedziczy po `Konto`), które dodatkowo zawiera pole `procentOszczednosci`.

Utworz klasę `KontoFirmowe`  (która dziedziczy po `Konto`), które dodatkowo zawiera pole `oplataZaTransakcje`.

 */
public class Konto {

    protected double stan = 0;
    protected int numer;
    protected Osoba wlasciciel;
    private static int liczbaKont = 0;

    Konto(Osoba wlasciciel) {
        this.numer = ++liczbaKont;
        this.wlasciciel = wlasciciel;
    }

    public void wplata(double kwota) {

        this.stan += kwota;

    }

    public boolean wyplata(double kwota) {

        if (this.stan < kwota) {
            return false;
        } else {
            this.stan -= kwota;
        }

        return true;

    }

    public String stan() {
        return "Stan konta to: " + stan;
    }

    public static boolean przelew (Konto nadawca, Konto odbiorca, int kwota) {

        if (nadawca.wyplata(kwota)) {
            odbiorca.wplata(kwota);
            return true;
        }

        return false;

    }

    @Override
    public String toString() {
        return String.format("Konto nr %s , osoba: %s, stan konta: %s", numer, this.wlasciciel, this.stan);
    }
}
