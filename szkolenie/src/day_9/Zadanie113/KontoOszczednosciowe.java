package day_9.Zadanie113;

import day_9.Zadanie112.Osoba;

public class KontoOszczednosciowe extends Konto{

    private double procentOszczednosci;

    KontoOszczednosciowe(Osoba wlasciciel, double procentOszczednosci) {

        super(wlasciciel);
        this.procentOszczednosci = procentOszczednosci;

    }

    @Override
    public void wplata(double kwota) {
        super.wplata(kwota + (kwota * procentOszczednosci));
    }
}
