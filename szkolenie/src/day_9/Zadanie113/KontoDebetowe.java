package day_9.Zadanie113;

import day_9.Zadanie112.Osoba;

public class KontoDebetowe extends Konto {

    private int limit;

    KontoDebetowe(Osoba wlasciciel, int limit) {
        super(wlasciciel);
        this.limit = limit;
    }

    @Override
    public boolean wyplata(double kwota) {

        boolean czyMozna = kwota < (this.stan + limit);
        if (czyMozna) {
            this.stan -= kwota;
        }

        return czyMozna;
    }
}
