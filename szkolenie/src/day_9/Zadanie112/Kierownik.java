package day_9.Zadanie112;

public class Kierownik extends Pracownik {

    private int premia;

    Kierownik(String imie, int wiek, int pensja, int premia) {
        super(imie, wiek, pensja);
        this.premia = premia;
    }

    public int getPremia() {
        return premia;
    }

    @Override
    public String toString() {
        return String.format("Jestem %s, mam %s lat. Pensja = %s a premia = %s", this.getImie(), this.getWiek(), this.getPensja(), premia);
    }

}
