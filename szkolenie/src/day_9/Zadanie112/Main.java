package day_9.Zadanie112;

public class Main {

    public static void main(String[] args) {
        Osoba osoba = new Osoba("Michal", 60);

        Kierownik kierownik = new Kierownik("Andrzej", 34, 5000, 2000);

        Pracownik pracownik = new Pracownik("Marek", 33, 2500);

//        System.out.println(osoba);
//        System.out.println(kierownik);
//        System.out.println(pracownik);

        Osoba[] ludki = new Osoba[3];

        ludki[0] = osoba;
        ludki[1] = pracownik;
        ludki[2] = kierownik;

        for (int i = 0; i < ludki.length; i++) {
            System.out.println(ludki[i]);
        }

        System.out.println(kosztRoczny(ludki));

    }


    public static int kosztRoczny(Osoba[] dane) {

        int totalCost = 0;

        for (int i = 0; i < dane.length; i++) {

            if (dane[i] instanceof Pracownik) {
                Pracownik p = (Pracownik) dane[i];
                totalCost += p.getPensja();
                if (dane[i] instanceof Kierownik) {
                    Kierownik k = (Kierownik) dane[i];
                    totalCost += k.getPremia();
                }
            }

        }

        return totalCost;

    }
}
