package day_9.Zadanie112;

public class Pracownik extends Osoba {

    private int pensja;

    Pracownik(String imie, int wiek, int pensja) {
        super(imie, wiek);
        this.pensja = pensja;
    }

    Pracownik(int pensja) {
        super("", 0);
        this.pensja = pensja;
    }

    public int getPensja() {
        return pensja;
    }

    @Override
    public String toString() {
        return String.format("Jestem %s, mam %s lat. Pensja = %s", this.getImie(), this.getWiek(), pensja);
    }

}
