package day_9.Zadanie112;

/*

*ZADANIE #112*
Utwórz klasę `Osoba`, która zawiera pola `imie`, `wiek`.

Następnie utwórz klasę `Pracownik`, która rozszerza klasę `Osoba` i zawiera dodatkowe pola `pensja`.

Następnie utwórz klasę `Kierownik`, która rozszerza klasę `Pracownik` i posiada dodatkowe pole `premia`.

Utwórz obiekty wszystkich klas oraz przetestuj działanie.

 */

public class Osoba {

    private String imie;
    private int wiek;

    public Osoba (String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    @Override
    public String toString() {
        return String.format("Jestem %s, mam %s lat", imie, wiek);
    }
}
