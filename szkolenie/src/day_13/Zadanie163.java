package day_13;

/**
 * *ZADANIE #163 - REKURENCJA*
 * Utwórz metodę, która przyjmie liczbę (jako `int`) a
 * następnie przy użyciu *REKURENCJI* policzy sumę cyfr.
 */

public class Zadanie163 {

    public static void main(String[] args) {

        System.out.println(sum(1001));

    }

    static int sum(int value) {
        System.out.println("przekazano " + value);

        if (value < 1) {
            return 0;
        } else {
            return (value % 10) + sum(value / 10);
        }

    }

}
