package day_13;

/**
 * *ZADANIE #162 - REKURENCJA*
 * Utwórz metodę, która przyjmie parametr określający liczbę wyświetleń napisu  `
 * "Hello World"`. Napis wyświetl przy użyciu *REUKRENCJI*
 */

public class Zadanie162 {

    public static void main(String[] args) {

        rekurencja(7);

    }

    static void rekurencja(int value) {

        if (value > 0) {
            System.out.println("Hello World ");
            rekurencja(--value);
        }

    }

}
