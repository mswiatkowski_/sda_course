package day_13.Zadanie165;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Samochod car1 = new Samochod("CW 123", Kolor.CZARNY);
        System.out.println(car1.hashCode());
        Samochod car2 = new Samochod("CW 123", Kolor.CZARNY);
        System.out.println(car2.hashCode());

        Samochod car3 = new Samochod("CWL 123", Kolor.CZERWONY);
        System.out.println(car3.hashCode());

        System.out.println(car1.equals(car2));
        System.out.println(car1.equals(car3));

        Set<Samochod> secik = new HashSet<>(
                Arrays.asList(
                        car1,
                        car2,
                        car3
                )
        );

        System.out.println(secik);

    }

}
