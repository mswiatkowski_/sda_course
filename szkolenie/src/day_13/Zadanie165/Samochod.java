package day_13.Zadanie165;

import java.util.Random;

/**
 * *ZADANIE #165*
 * Utwórz klasę `Samochod` (z polami `nrRejestracji` oraz `kolor`)
 * oraz zaimplementuj w niej meotdę `equals()` i `hashcode()` która umożliwi
 * porównanie obiektów ze sobą. Przetestuj działanie tej metody tworząc kilka obiektów i porównując je ze sobą.
 */

public class Samochod {

    private String nrRejestracji;
    private Kolor kolor;

    Samochod(String nrRejestracji, Kolor kolor) {
        this.nrRejestracji = nrRejestracji;
        this.kolor = kolor;
    }

    public String getNrRejestracji() {
        return nrRejestracji;
    }

    public Kolor getKolor() {
        return kolor;
    }

    @Override
    public String toString() {
        return "Samochod{" +
                "nrRejestracji='" + nrRejestracji + '\'' +
                ", kolor=" + kolor +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Samochod)) {
            return false;
        }

        Samochod samochod = (Samochod) obj;

        if (!samochod.nrRejestracji.equals(this.nrRejestracji)) {
            return false;
        }

        if (!samochod.kolor.equals(this.kolor)) {
            return false;
        }

        return true;

    }

    @Override
    public int hashCode() {
        int tmp = 33;
        tmp += nrRejestracji.hashCode() * 31;
        tmp += kolor.hashCode() * 31;
        return tmp;

//        return new Random().nextInt();

    }

}

enum Kolor {
    BIALY, ZIELONY, CZERWONY, CZARNY;
}
