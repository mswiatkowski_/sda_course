package day_13;

/**
 * Utwórz metodę, która przyjmie String jako parametr i sprawdzi, przy użyciu *REKURENCJ*, czy parametr jest PALINDROMEM. (edited)
 */

public class Zadanie164 {

    public static void main(String[] args) {

        System.out.println(palindrom("ślimak"));

    }

    static boolean palindrom(String word) {

        if (word.length() <= 1) {
            return true;
        }

        if (word.charAt(0) == word.charAt(word.length() - 1)) {
            return palindrom(word.substring(1, word.length() - 1));
        }

        return false;

    }
}
