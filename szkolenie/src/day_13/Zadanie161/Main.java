package day_13.Zadanie161;
import zad161.Wiadomosc;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Wiadomosc wiadomosc = new Wiadomosc("Dziś wcale nie jest tak ciepło", "Platon");
//        saveToFile(wiadomosc);

        System.out.println(getFile("files/wiadomosc.txt").getTresc());
    }

    private static Wiadomosc getFile(String path) throws IOException, ClassNotFoundException {

        FileInputStream input = new FileInputStream(path);
        ObjectInputStream object = new ObjectInputStream(input);

        return (Wiadomosc) object.readObject();

    }

    private static void saveToFile(Wiadomosc wiadomosc) throws IOException {

        FileOutputStream buff = new FileOutputStream("files/zad161");
        ObjectOutputStream stream = new ObjectOutputStream(buff);

        stream.writeObject(wiadomosc);
        stream.close();

    }

}
