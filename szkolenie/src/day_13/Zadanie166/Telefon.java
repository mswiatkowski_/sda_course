package day_13.Zadanie166;

import java.util.Objects;

/**
 * *ZADANIE #166*
 * Utwórz klasę `Telefon` (z polami `nazwa`, `model`,  `marka`, `numer`) oraz zaimplementuj w niej meotdę `equals()` i `hashCode()`
 */

public class Telefon {

    private String nazwa;
    private String model;
    private String marka;
    private int numer;

    public Telefon(String nazwa, String model, String marka, int numer) {
        this.nazwa = nazwa;
        this.model = model;
        this.marka = marka;
        this.numer = numer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Telefon telefon = (Telefon) o;
        return numer == telefon.numer &&
                Objects.equals(nazwa, telefon.nazwa) &&
                Objects.equals(model, telefon.model) &&
                Objects.equals(marka, telefon.marka);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwa, model, marka, numer);
    }

}
